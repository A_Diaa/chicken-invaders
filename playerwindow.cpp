//
//  playerwindow.cpp
//  sfml trials
//
//  Created by mariam hegazy on 4/22/17.
//  Copyright © 2017 mariam hegazy. All rights reserved.
//

#include <stdio.h>
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include<SFML/Window.hpp>
#include <iostream>
#include "object.h"
#include "bullet.h"
#include "rocket.h"
#include "chicken.h"
#include "rock.h"
#include "player.h"
#include <vector>
#include <string>
#include "ResourcePath.hpp"
using namespace std;
using namespace sf;
const int width = 1080,length = 1920;
const float rectW = 50.0, rectL = 300.0;
const float RECT_POS_X = length/2.0 - rectL/2.0;
const float RECT_POS_Y = width/2.0 - rectW/2.0 - 50;
const float RECTP_POS_X = RECT_POS_X;
const float RECTP_POS_Y = RECT_POS_Y + rectW + 20;
player newplayer;
int main()
{
    RenderWindow window(VideoMode(length,width), "welcome window");
    string nameText,passText,asterisk;
    Vector2i mousepos;
    bool flagU = false;
    bool flagP = false;
    bool success = false,signIn;
    
    //username text
    Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    //code for pc
#endif
    //enter button
    RectangleShape button(Vector2f(120.0,75.0));
    button.setPosition(RECTP_POS_X + 80, RECT_POS_Y + 150);
    button.setFillColor(Color::Blue);
    Text textB("okay",font,40);
    textB.setPosition(RECTP_POS_X + 90, RECT_POS_Y + 160);
    
    Text text("username", font, 40);
    text.setColor(Color::Blue);
    text.setPosition(Vector2f(RECT_POS_X,RECT_POS_Y));
    
    //password text
    Text textP("password", font, 40);
    textP.setColor(Color::Blue);
    textP.setPosition(Vector2f(RECTP_POS_X,RECTP_POS_Y));
    
    //username rect
    RectangleShape rect(Vector2f(rectL, rectW));
    rect.setPosition(Vector2f(RECT_POS_X,RECT_POS_Y));
    rect.setFillColor(Color::White);
    
    //password rect
    RectangleShape rectP(Vector2f(rectL, rectW));
    rectP.setPosition(Vector2f(RECTP_POS_X ,RECTP_POS_Y));
    rectP.setFillColor(Color::White);
    
    //error text
    Text errorText("",font,24);
    errorText.setColor(Color::White);
    string error;
    
    //back button
    RectangleShape back(Vector2f(120,75));
    back.setPosition(length - 120,width - 75);
    back.setFillColor(Color::Blue);
    Text backText("back",font,40);
    backText.setPosition(length - 100,width - 70);
    backText.setFillColor(Color :: White);
    
    while(window.isOpen())
    {
        
        while(!success)
        {
            Event Event;
            while (window.pollEvent(Event))
            {
                if (Event.type == Event::Closed)
                    window.close();
                
                if (Event.type == Event::MouseButtonPressed)
                {
                    if (Event.mouseButton.button == Mouse::Left)
                    {
                        mousepos = Mouse :: getPosition(window);
                        if(rect.getGlobalBounds().contains(mousepos.x,mousepos.y))
                        {
                            flagU = true;
                            text.setString("");
                            flagP = false;
                            
                        }
                        if(rectP.getGlobalBounds().contains(mousepos.x,mousepos.y))
                        {
                            flagP = true;
                            textP.setString("");
                            flagU = false;
                            
                        }
                        if(button.getGlobalBounds().contains(mousepos.x,mousepos.y))
                        {
                            signIn = true;
                            success = newplayer.setData(nameText,passText,signIn,error);
                            
                            text.setString("username");
                            textP.setString("password");

                        }
                        if(back.getGlobalBounds().contains(mousepos.x,mousepos.y))
                        {
                            //state = menu;
                        }
                        
                    }
                    
                }
                
                if(flagP)
                {
                    if(Event.type == Event::TextEntered)
                        if ((Event.text.unicode < 127) && (Event.text.unicode != 8) && (Event.text.unicode != 10))
                        {
                            cout << (char)Event.text.unicode << endl;
                            passText += (char)(Event.text.unicode);
                            asterisk += '*';
                            textP.setString(asterisk);
                        }
                    if (Event.type == Event::KeyPressed)
                        if (Event.key.code == Keyboard::BackSpace)
                        {  passText.erase(passText.length()-1,1);
                            asterisk.erase(asterisk.length()-1,1);
                            textP.setString(asterisk);
                        }
                }
                else
                {
                    if((Event.type == Event::TextEntered)&&(flagU))
                    {
                        if ((Event.text.unicode < 127) && (Event.text.unicode != 8) && (Event.text.unicode != 10))
                        {
                            cout << (char)Event.text.unicode << endl;
                            nameText += (char)(Event.text.unicode);
                            text.setString(nameText);
                        }
                    }
                    if ((Event.type == Event::KeyPressed) && (flagU))
                        if (Event.key.code == Keyboard::BackSpace)
                        {  nameText.erase(nameText.length()-1,1);
                            text.setString(nameText);
                        }
                }
                
                if(Event.type == Event::KeyPressed)
                    if(Event.key.code == Keyboard::Return)
                    {
                        signIn = true;
                        success = newplayer.setData(nameText,passText,signIn,error);
                        text.setString("username");
                        textP.setString("password");

                    }
            }
            if((!success) && (signIn))
            {
                errorText.setString(error);
            }
            else
                if((!success) && (!signIn))
                {
                    errorText.setString(error);
                }
            
            if(success)
            {
                window.close();
            }
            window.clear();
            window.draw(rect);
            window.draw(rectP);
            window.draw(button);
            window.draw(back);
            window.draw(text);
            window.draw(textP);
            window.draw(textB);
            window.draw(errorText);
            window.draw(backText);
            window.display();
        }
    }

    
    return 0;
}
