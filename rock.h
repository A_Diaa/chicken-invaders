#pragma once
#ifndef rock_h
#define rock_h
#include <iostream>
#include <SFML/Graphics.hpp>
#include "object.h"
//here
// 1 is splittable  
class rock : public object
{
private:
    bool type;
public:
    rock(Texture &);
    rock(bool, Texture &);
	rock();
    void setType(bool, Texture &); //sets rocktype to be an asteroid or meteor
    bool isSplitable(); //checks if rock is an asteroid
    bool divisible(); //if a certain amount of lifepoints are down,flag is up...it can be quantized...flag is true if life points = 50 || 75 || 100
    void releasePowerup (vector <object> &, Texture &);// adds a powerup to main powerUp board vector after setting position.
    void divideRock(vector <object> &); // if isdivide is true, deletes rock and generates 2 smaller rocks each with quarter the life points and half size
    void updateRock();
};
#endif // !rock_h
