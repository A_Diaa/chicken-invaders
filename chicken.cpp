#include "chicken.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include "object.h"
#include <cstdlib>
using namespace std;
using namespace sf;

chicken::chicken()
{

}

/*
chicken:: chicken(Ctype typ)
{
chicken::setType(typ);
}
*/


void  chicken::setType(Ctype typ, Texture &chicken1T, Texture &chicken2T, Texture &chicken3T, Texture & chicken4T) //sets shape, size, color, speed, direction, texture, position, prize probabilities of chicken according to its enum Ctype a.
{
    counter = 0;
       switch(typ)
    {
        case mother:
        {
	      type = mother;
          object::setTexture(chicken3T);
          object::setLP(MOTHER_LP);
          object::setVal(MOTHER_VAL);
          object::setSpeed(MOTHER_SPEED);
          object::setDmg(CHICKEN_DMG);
          object::setSize(MOTHER_SIZE);
		  object::setOrigin(object::getLocalBounds().width / 2, object::getLocalBounds().height / 2);

            powerUp_ = MOTHER_POWERUP_PROB;
            meat_ = MOTHER_MEAT_PROB;
            coins_ = MOTHER_COINS_PROB;
            hearts_ = MOTHER_HEART_PROB;
			egg_ = MOTHER_EGG_PROB;
			immunity_ = MOTHER_IMMUNITY_PROB;
			break;
        }

        case normal:
        {
			type = normal;
			object::setTexture(chicken1T);
            object::setLP(NORMAL_LP);
            object::setVal(NORMAL_VAL);
			object::setSpeed(NORMAL_SPEED);
            object::setDmg(CHICKEN_DMG);
            object::setSize(NORMAL_SIZE);
			object::setOrigin(object::getLocalBounds().width / 2, object::getLocalBounds().height / 2);

            powerUp_ = NORMAL_POWERUP_PROB;
            meat_ = NORMAL_MEAT_PROB;
            coins_ = NORMAL_COINS_PROB;
            hearts_ = NORMAL_HEART_PROB;
			egg_ = NORMAL_EGG_PROB;
			immunity_ = NORMAL_IMMUNITY_PROB;

			break;
        }

        case trooper:
        {
			type = trooper;
			object::setTexture(chicken4T);
			//object::setColor(Color::Green);
            object::setLP(TROOPER_LP);
            object::setVal(TROOPERL_VAL);
            object::setDmg(CHICKEN_DMG);
            object::setSpeed(TROOPER_SPEED);
            object::setSize(TROOPER_SIZE);
			object::setOrigin(object::getLocalBounds().width / 2, object::getLocalBounds().height / 2);

            powerUp_ = TROOPER_POWERUP_PROB;
            meat_ = TROOPER_MEAT_PROB;
            coins_ = TROOPER_COINS_PROB;
            hearts_ = TROOPER_HEART_PROB;
			egg_ = TROOPER_EGG_PROB;
			immunity_ = TROOPER_IMMUNITY_PROB;

			break;
        }

        
    }
}

void chicken:: updateChicken(Texture & chicken1T, Texture & chicken2T, Texture & chicken3T, Texture & chicken4T, Texture & chicken5T)
{
    if(counter < 19)
        counter++;
    else
        counter = 0;
    switch(type)
    {
        case mother:
		{ 
			if (counter < 10)
				object::rotate(1);
			else
				object::rotate(-1);
			break;
		}
        case normal:
		{
			if (counter < 10)
				object::setTexture(chicken1T);
			else
				object::setTexture(chicken2T);
			break;
		}
        case trooper:
		{ 
			if (counter < 10)
				object::setTexture(chicken4T);
			else
				object::setTexture(chicken5T);
			break;
		}

    }
    
}

bool chicken::drop(vector <object> & prizeBoard, vector <object> & eggBoard, Texture & eggT, Texture & powerT, Texture & immuneT, Texture & heartT, Texture & coinT, Texture & foodT) //according to probability of prizes,this function determines the prize after a chickenís death and calls releasing functions
{
	if (chicken::getType() == mother)
	{
		int probability = rand() % 10000;
		if (probability<egg_)
			chicken::releaseEgg(eggBoard, eggT);
		else if (probability < powerUp_)
			chicken::releasePowerup(prizeBoard, powerT);
		else if (probability < hearts_)
			chicken::releaseHeart(prizeBoard, heartT);
		else if (probability < coins_)
			chicken::releaseCoin(prizeBoard, coinT);
		else if (probability < immunity_)
			chicken::releaseImmunity(prizeBoard, immuneT);

	}

	else
	{
		if (object::isDead())
		{

			chicken::releaseMeat(prizeBoard, foodT);
			int probability = rand() % 100;
			if (probability < powerUp_)
				chicken::releasePowerup(prizeBoard, powerT);
			else if (probability < hearts_)
				chicken::releaseHeart(prizeBoard, heartT);
			else if (probability < coins_)
				chicken::releaseCoin(prizeBoard, coinT);
			else if (probability < immunity_)
				chicken::releaseImmunity(prizeBoard, immuneT);
		}

		else
		{
			int probability = rand() % 10000;
			if (probability < egg_)
			{
				chicken::releaseEgg(eggBoard, eggT);
				return true;
			}
		}
	}
	return false;
}

void chicken:: releasePowerup(vector <object> & powerBoard, Texture & powerT)// adds a powerup to main powerUp board vector after setting position
{
    object powerUp;
    powerUp.object::setTexture(powerT);
	powerUp.setPrizeType(power);
    Vector2f position;
    position = getPosition();
    powerUp.setPosition(position);
    powerBoard.push_back(powerUp);
}

void chicken::releaseImmunity(vector <object> &immunityBoard, Texture & immuneT) //releases immunity powerUp
{
	object immunity;
	immunity.object::setTexture(immuneT);
	immunity.setPrizeType(immune);
	Vector2f position;
	position = getPosition();
	immunity.setPosition(position);
	immunityBoard.push_back(immunity);
}


void chicken:: releaseMeat(vector <object> & meatBoard, Texture & foodT) // adds a piece of meat to main meat board vector after setting position
{
	int num = rand() % 2;
    object meat;
    meat.object::setTexture(foodT);
	meat.setPrizeType(food);
    Vector2f position;
    position = getPosition();
    meat.setPosition(position);
	if (num)
		meat.setSpeed(Vector2f(rand() % 200 + 150, 250));
	else
		meat.setSpeed(Vector2f(-rand() % 200 - 150, 250));

    meatBoard.push_back(meat);
}

void chicken:: releaseCoin(vector <object> & coinBoard, Texture & coinT)//adds a coin to main coin board vector after setting position

{
	object coin;
    coin.object::setTexture(coinT);
	coin.setPrizeType(money);
    Vector2f position;
    position = getPosition();
    coin.setPosition(position);
    coinBoard.push_back(coin);
}

void chicken:: releaseEgg(vector <object> & eggBoard, Texture & eggT)// takes in command to create an egg and release it to main boardEggs vector after setting position
{
    object egg;
    egg.setLP(DROPS_LP);
    egg.setVal(0); //ay constant
    egg.setDmg(ROCKET_LIFE_POINTS);
    egg.object::setTexture(eggT);
	egg.setSize(EGG_SIZEX, EGG_SIZEY);
    int eggSpeed = rand()%EGG_SPEED_RANGE+EGG_MIN_SPEED;
    
    if(chicken::getType() == mother)
    {
        int direction = rand() % 3;
        switch(direction)
        {
            case 2:
                egg.setSpeed(Vector2f(rand()%(2* EGG_MIN_SPEED)- EGG_MIN_SPEED,rand() % EGG_SPEED_RANGE + EGG_MIN_SPEED));
                break;
            case 1:
                egg.setSpeed(Vector2f(rand() % EGG_SPEED_RANGE + EGG_MIN_SPEED,rand() % EGG_SPEED_RANGE + EGG_MIN_SPEED));
                break;
            case 0:
                egg.setSpeed(Vector2f(- rand() % EGG_SPEED_RANGE - EGG_MIN_SPEED,rand() % EGG_SPEED_RANGE + EGG_MIN_SPEED));
                break;
        }
        egg.setPosition(chicken::getGlobalBounds().left + chicken::getGlobalBounds().width/2.f,  chicken:: getGlobalBounds().height + chicken:: getGlobalBounds().top);
        
    }
    else
    {
        egg.setSpeed(Vector2f(0,eggSpeed));
        Vector2f position;
        position = chicken::getPosition();
        egg.setPosition(position);
    }
    eggBoard.push_back(egg);
}

void chicken:: releaseHeart(vector <object> & heartBoard, Texture & heartT)// takes in command to create an egg and release it to main boardEggs vector after setting position
{
    object heart;
    heart.object::setTexture(heartT);
	heart.setPrizeType(life);
    Vector2f position;
    position = getPosition();
    heart.setPosition(position);
    heartBoard.push_back(heart);
}

Ctype chicken:: getType()
{
    return type;
}

