#include <SFML/Graphics.hpp>
#include "object.h"
#include <iostream>
#include <string>
#include <climits>

using namespace std;
using namespace sf;

object::object(int lp, int val, int lvl, int dmg, Vector2f Speed, int Size, Texture txt)
{
	//certain object
	object::setLP(lp);
	object::setVal(val);
	object::setLvl(lvl);
	object::setDmg(dmg);
	object::setSpeed(Speed);
	object::setSize(Size);
	object::object::setTexture(txt);
	active = true;
}

object::object()
{
	//default object
	object::setLP(INT_MAX);
	object::setVal(0);
	object::setLvl(0);
	object::setDmg(0);
	object::setSpeed(Vector2f(50, 50));
	object::setSize(100);
	//object::object::setTexture("default.png");
}

void object::setLP(int lp) //sets lifepoints
{
	if (lp >= 0)
		lifepoints = lp;
}

void object::setVal(int val) //sets value
{
	if (val >= 0)
		value = val;
}
void object::setLvl(int lvl) //sets level
{
	if (lvl >= 0)
		level = lvl;
}

void object::setDmg(int dmg) //sets damage
{
	if (dmg >= 0)
		damage = dmg;
}
void object::setSpeed(Vector2f Speed) //sets speed
{
		speed = Speed;
}
void object::setSize(float sizex, float sizey) //sets absolute size
{
	if (sizex > 0 && sizey>0)
	{
		object::setScale(sizex / Sprite::getLocalBounds().width, sizey / Sprite::getLocalBounds().height);
	}
}
void object::setGSize(float sizex, float sizey) //sets absolute size
{
	if (sizex > 0 && sizey>0)
	{
		object::setScale(sizex / Sprite::getGlobalBounds().width, sizey / Sprite::getGlobalBounds().height);
	}
}

void object::setSize(int squareSize) //sets absolute size and rescales as a square
{
	if (squareSize> 0)
	{
		object::setScale(float(squareSize) / object::getLocalBounds().width, float(squareSize) / object::getLocalBounds().height);
	}
}

float object::getSize() const
{
	return object::getGlobalBounds().width;
}

void object::activate() //sets if object is active (ie: on Board)
{
	active = true;
}

void object::deactivate()
{
	active = false;
}

void object::setImg(Texture &txt)//sets texture of sprite
{
	txt.setSmooth(true);
	object::setTexture(txt);
}

void object::setPrizeType(Ptype chosen)
{
	prizeType = chosen;
	switch (chosen)
	{
	case power: 
	{
		object::setLP(DROPS_LP);
		object::setVal(POWERUP_VAL); //ay constant
		object::setDmg(0);
		object::setSpeed(DROPS_SPEED);
		object::setSize(25);
		break;
	}
	case food:
	{
		object::setLP(DROPS_LP);
		object::setVal(MEAT_VAL); //ay constant
		object::setDmg(0);
		object::setSpeed(DROPS_SPEED);
		object::setSize(25);
		break;
	}
	case money:
	{
		object::setLP(DROPS_LP);
		object::setVal(COIN_VAL); //ay constant
		object::setDmg(0);
		object::setSpeed(DROPS_SPEED);
		object::setSize(25);

		break;
	}
	case immune:
	{
		object::setLP(DROPS_LP);
		object::setVal(IMMUNITY_VAL); //ay constant
		object::setDmg(0);
		object::setSpeed(DROPS_SPEED);
		object::setSize(25);
		break;
	}
	case life:
	{
		object::setLP(DROPS_LP);
		object::setVal(1); //ay constant
		object::setDmg(0);
		object::setSpeed(DROPS_SPEED);
		object::setSize(25);
		break;
	}
	default: 
		break;
	}
}

void object::levelUp()
{
	level++;
}

void object::IDamage()
{
	damage += 50;
}

void object::DDamage()
{
	damage -= 50;
}

void object::takeDmg(object param)
{
	if (param.damage <= lifepoints)
		lifepoints -= param.damage;
	else
		lifepoints = 0;
}

int object::getLP() const
{
	return lifepoints;
}

int object::getVal() const
{
	return value;
}

int object::getLvl() const
{
	return level;
}

int object::getDmg() const
{
	return damage;
}

Ptype object::getPrizeType() const
{
	return prizeType;
}

Vector2f object::getSpeed() const
{
	return speed;
}


bool object::isActive() const
{
	return active;
}

bool object::isHit(object param) const
{
	bool flag;
	FloatRect myBounds = object::getGlobalBounds();
	FloatRect paramBounds = param.getGlobalBounds();
	if (myBounds.intersects(paramBounds))
		flag = true;
	else
		flag = false;
	return flag;
}

bool object::isDead() const
{
	bool flag;
	if (lifepoints <= 0)
		flag = true;
	else
		flag = false;
	return flag;
}

bool object::outBounds(FloatRect windowBounds)
{
	bool flag;
	FloatRect myBounds = object::getGlobalBounds();
	if (!myBounds.intersects(windowBounds))
		flag = true;
	else 
		flag = false;
	return flag;
}
