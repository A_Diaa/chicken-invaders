#pragma once
#ifndef chicken_h
#define chicken_h
#include <iostream>
#include <SFML/Graphics.hpp>
#include "object.h"
//here

class chicken : public object
{
private:
    int powerUp_, meat_,coins_,hearts_; //probabilities of their release after death of a chicken
    int difficulty;
    int egg_;
	int immunity_;
    Ctype type;
public:
    chicken();
   // chicken(Ctype);
    void setType(Ctype, Texture &, Texture &, Texture &, Texture &); //sets shape, size, color, speed, direction, texture, position, prize probabilities of chicken according to its enum Ctype a.
    bool drop(vector <object> & prizeBoard, vector <object> & eggBoard, Texture &, Texture &, Texture &, Texture &, Texture &, Texture &); //according to probability of prizes,this function determines the prize after a chicken’s death and calls releasing functions
    void releasePowerup(vector <object> &, Texture &);// adds a powerup to main powerUp board vector after setting position
    void releaseMeat(vector <object> &, Texture &); // adds a piece of meat to main meat board vector after setting position
    void releaseCoin(vector <object> &, Texture &); //adds a coin to main coin board vector after setting position
    void releaseEgg(vector <object> &, Texture &); // takes in command to create an egg and release it to main boardEggs vector after setting position
    void releaseHeart(vector <object> &, Texture &); //releases heart powwr up for use only after death of a chicken
	void releaseImmunity(vector <object> &, Texture &); //releases immunity powerUp
	Ctype getType();
    void updateChicken(Texture & chicken1T, Texture & chicken2T, Texture & chicken3T, Texture & chicken4T, Texture & chicken5T);
    
};
#endif // !chicken_h
