#pragma once
#ifndef back_h
#define back_h
#ifdef __APPLE__
#include "SFML/Graphics.hpp"
#else
#include "SFML\Graphics.hpp"
#endif
#include <iostream>
using namespace std;
using namespace sf;

class back
{
private : 
	Texture backText; 
	Sprite backSprite; 
	float backY; 
	float backSpeed;
public:
	back();
	void spriteUp(float); 
	void drawOn(RenderWindow&);

};
#endif // !back_h
