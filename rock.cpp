#include "rock.h"
#include <iostream>
#include <SFML/Graphics.hpp>
#include "object.h"
using namespace std;
using namespace sf;
rock:: rock(Texture & rockT)
{

    object::setLP(ROCK_LP);
    object::setDmg(ROCK_DMG);
    object::setVal(ROCK_VAL);
    object::setSpeed(ROCK_SPEED);
	object::setImg(rockT);
    object::setSize(rand() % ROCK_SIZERANGE + ROCK_MINSIZE);
    type = false; //by default
}

rock::rock() //no image has to set image and then size later
{
	object::setLP(ROCK_LP);
	object::setDmg(ROCK_DMG);
	object::setVal(ROCK_VAL);
	object::setSpeed(ROCK_SPEED);
	object::setSize(rand() % ROCK_SIZERANGE + ROCK_MINSIZE);
	type = false; //by default
}

rock::rock(bool typ, Texture &rockT)
{
    type = typ;
    object::setLP(ROCK_LP);
    object::setDmg(ROCK_DMG);
    object::setVal(ROCK_VAL);
    object::setSpeed(ROCK_SPEED);
	object::setImg(rockT);
    object::setSize(rand() % ROCK_SIZERANGE + ROCK_MINSIZE);
}

void rock:: setType(bool typ, Texture & rockT) //sets rocktype to be an asteroid or meteor
{
    type = typ;
    object::setLP(ROCK_LP);
    object::setDmg(ROCK_DMG);
    object::setVal(ROCK_VAL);
    object::setSpeed(Vector2f(-rand()%125-25,100.f));
	object::setImg(rockT);
    object::setSize(ROCK_MINSIZE);
}

bool rock::isSplitable() //checks if rock is an asteroid..getter
{
    return type;
}

bool rock::divisible() //if a certain amount of lifepoints are down,flag is up...it can be quantized...flag is true if life points = 50 || 75 || 100
{
	int lp = rock::getLP();
    if((lp == ROCK_LP_L1)|| lp == (ROCK_LP_L2) || lp == (ROCK_LP_L3))
        return true;
    else
        return false;
}

void rock::releasePowerup (vector <object> & powerBoard, Texture & powerT)// adds a powerup to main powerUp board vector after setting position.
{
	object powerUp;
	powerUp.object::setTexture(powerT);
	powerUp.setPrizeType(power);
	Vector2f position;
	position = getPosition();
	powerUp.setPosition(position);
	powerBoard.push_back(powerUp);
}
void rock::divideRock(vector <object> & rockBoard) // if isdivide is true, deletes rock and generates 2 smaller rocks each with quarter the life points and half size
{
    if(rock:: isSplitable())
    if(rock::divisible())
    {
        rock newRock;
        Vector2f position = object::getPosition();
        float size = object::getSize()/2.f;
        object:: setSize(int(size));
        newRock.setSize(int(size));
        newRock.setPosition(position);
        rockBoard.push_back(newRock);
    }
}

void rock::updateRock()
{
	object::rotate(-45 / RAD_TO_DEG);
}
