#ifdef __APPLE__
#include "SFML/Graphics.hpp"
#else
#include "SFML\Graphics.hpp"
#endif
#include "back.h"
using namespace sf;


back::back()
{
	backSpeed = 300.f;
#ifdef __APPLE__
    if (!backText.loadFromFile("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/Assets/photos/background.png"))
        cout << "Error loading background image" << endl;
#else
	if (!backText.loadFromFile("background.png"))
		cout << "Error loading background image" << endl;
#endif

	backText.setRepeated(true);
	backSprite.setTexture(backText);
	backY = 0.f;
}

void back::spriteUp(float frametime)
{
	backY -= backSpeed*frametime;
	backSprite.setTextureRect(IntRect(0.f, backY, 1920.f, 1080.f));
}

void back::drawOn(RenderWindow & window)
{
	window.draw(backSprite);
}
