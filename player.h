#pragma once
#ifndef player_h
#define player_h
#include <iostream>
#include <sfml/Graphics.hpp>
#include <string>
using namespace std;
class player 
{
private:
	string username, password;
	int score, coins, meat;
	bool rockets[3];
	bool weapons[3];

public:
	player();
	//player(string, string, bool); // true for sign in
	void addScore(unsigned int); //takes unsigned int and adds to score
	void addCoins(int);  //takes value (signed)  and adds (or deducts on purchase) from coins
	void addFood(int); //takes value (signed)  and adds (or deducts on purchase) from food
	void updateFile(); //player’s file with new score, food, and coins
	//void setRocket(int);
	bool setUname(string);  //sets new username for player
	bool setPassword(string); //sets new password for player
    void setScore(int); //sets score 
	bool checkCredentials(string,string); //checks if the player has access rights
	bool addPlayer(string); //checks availability of username only
	bool vallidation(string); //space vallidation
    bool setData(string,string,bool,string&);
	bool buyR(int rocketNo); 
	bool buyW(int weaponNo); 
	bool isRocketBought(int rocketNo);
	bool isWeaponBought(int weaponNo);
    int getScore() const;
    int getMeat() const;
    int getCoins() const;
    string getUsername() const;
    string getPassword() const;
    
};
#endif // !player_h
