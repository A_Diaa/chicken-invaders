#include "object.h"
#include "rocket.h"
#include <iostream>
#include <math.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
using namespace std;
using namespace sf;



rocket::rocket()
{
	lifepoints = ROCKET_LIFE_POINTS;
	lives = 3;
	weaponPower = MIN_WEAPON_POWER;
	rocket::setSpeed(ROCKET_SPEED);
	object::setDmg(BULLET_DAMAGE);
}

bool rocket::respawn()
{
	object::activate();
	object::setLP(ROCKET_LIFE_POINTS);
	return true;
}

void rocket::fire(vector <bullet> &bulletBoard) //by reference
{
	if (weaponType == lightning) //special weapon
	{
		for (int i = -weaponPower; i <= weaponPower; i++)
		{
			bulletBoard.push_back(bullet());
			bulletBoard.at(bulletBoard.size() - 1).setSpeed(STATIONARY);
			bulletBoard.at(bulletBoard.size() - 1).setPosition(rocket::getPosition().x, rocket::getPosition().y);
			bulletBoard.at(bulletBoard.size() - 1).setRotation(RAD_TO_DEG*atan(-i*BULLET_DEVIATION_ROT)+180);
			if (object::getDmg() == BULLET_DAMAGE_HIGH)
			{
				bulletBoard.at(bulletBoard.size() - 1).setDmg(BULLET_DAMAGE_HIGH);
				bulletBoard.at(bulletBoard.size() - 1).setColor(Color::Magenta);
			}
			//bulletBoard.at(bulletBoard.size() - 1).setType(weaponType);
		}
	}
	else
		for (int i = -weaponPower; i <= weaponPower; i++)
		{
			bulletBoard.push_back(bullet());
			bulletBoard.at(bulletBoard.size() - 1).setSpeed(Vector2f(i*BULLET_DEVIATION_VEL, BULLET_YGAIN));
			bulletBoard.at(bulletBoard.size() - 1).setPosition(rocket::getPosition().x+20.f, rocket::getPosition().y-rocket::getGlobalBounds().height/2);
			bulletBoard.at(bulletBoard.size() - 1).setRotation(RAD_TO_DEG*atan(-i*BULLET_DEVIATION_ROT) + 180);
			if (object::getDmg() == BULLET_DAMAGE_HIGH)
			{
				bulletBoard.at(bulletBoard.size() - 1).setDmg(BULLET_DAMAGE_HIGH);
				bulletBoard.at(bulletBoard.size() - 1).setColor(Color::Magenta);
			}
			//bulletBoard.at(bulletBoard.size() - 1).setType(weaponType);
		}
	//for (int i = 0; i < bulletBoard.size(); i++)//loop for setting the type
	//	bulletBoard[i].setType(weaponType);//setting the type
}

void rocket::IWP()
{
	if (weaponPower < MAX_WEAPON_POWER)
		weaponPower++;
	else
		object::setDmg(BULLET_DAMAGE_HIGH);
}

void rocket::DWP()
{
	if (object::getDmg()==BULLET_DAMAGE_HIGH)
		object::setDmg(BULLET_DAMAGE);

	else if (weaponPower > MIN_WEAPON_POWER)
		weaponPower--;
}

void rocket::oneUp()
{
		lives++;
}

void rocket::die()
{
	lifepoints = 0;
	lives--;
	rocket::DWP();
	object::deactivate();
}

bool rocket::gameOver() const
{
	bool flag;
	if (lives <= 0)
		flag = true;
	else
		flag = false;
	return flag;
}

void rocket::setWT(Wtype chosen)
{
	weaponType = chosen;
}

void rocket::setRT(Rtype chosen, Texture& rocket1, Texture& rocket2, Texture& rocket3)
{
	rocketType = chosen;
	switch (chosen)
	{
	case millitary:
        rocket::setTexture(rocket1);
		rocket::setSize(90.f, 120.f);
		rocket::setOrigin(rocket::getLocalBounds().width/2,rocket::getLocalBounds().height/2);
		break;
            
	case explorer:
        rocket::setTexture(rocket2);
        rocket::setSize(90.f, 120.f);
		rocket::setOrigin(rocket::getLocalBounds().width / 2, rocket::getLocalBounds().height / 2);
		break;
            
	case alien: 
		rocket::setTexture(rocket3);
		rocket::setSize(90.f, 120.f);
		rocket::setOrigin(rocket::getLocalBounds().width / 2, rocket::getLocalBounds().height / 2);
		break;
	}
}

Wtype rocket::getWT() const
{
	return weaponType;
}

Rtype rocket::getRT() const
{
	return rocketType;
}

bool rocket::isImmune() const
{
	return immune;
}

void rocket::Immune()
{
	immune = true;
}

void rocket::deImmune()
{
	immune = false;
}

int rocket::getLives() const
{
	return lives;
}

void rocket::minPower()
{
	weaponPower = 0;
}

void rocket::setLives(int Lives)
{
	lives = Lives;
}