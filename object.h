#pragma once
#ifndef object_h
#define object_h
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
using namespace std;
using namespace sf;

const Vector2f STATIONARY(0.f, 0.f);

//Shop prices
const int ROCKET1_PRICE = 10000; 
const int ROCKET2_PRICE = 20000; 
const int ROCKET3_PRICE = 30000;

const int WEAPON1_PRICE = 10000;
const int WEAPON2_PRICE = 20000;
const int WEAPON3_PRICE = 30000;

//Rock constants
const int ROCK_LP = 300;
const int ROCK_LP_L1 = 100;
const int ROCK_LP_L2 = 75;
const int ROCK_LP_L3 = 50;
const int ROCK_VAL = 100;
const Vector2f ROCK_SPEED = Vector2f(-100,100);
const int ROCK_MINSIZE = 20;
const int ROCK_SIZERANGE = 130;

//Drops constnts
const Vector2f DROPS_SPEED = Vector2f(0, 200.f);
const int DROPS_LP = 50;
const int POWERUP_VAL = 80;
const Vector2f POWER_SIZE = Vector2f(20.f, 50.f);
const int MEAT_VAL = 40;
const int COIN_VAL = 60;
const int IMMUNITY_VAL = 100;

//Egg constants
const int EGG_SPEED_RANGE = 250;
const int EGG_MIN_SPEED = 150;
const int EGG_SIZE = 30;
const float EGG_SIZEX = 22.f;
const float EGG_SIZEY = 35.f;

//Normal chicken constants
const int NORMAL_MEAT_PROB = 100;
const int NORMAL_POWERUP_PROB = 5;
const int NORMAL_HEART_PROB = 7;
const int NORMAL_COINS_PROB = 20;
const int NORMAL_IMMUNITY_PROB = 25;
const int NORMAL_EGG_PROB = 5;
const int NORMAL_LP = 150;
const int NORMAL_VAL = 30;
const int NORMAL_SIZE = 30;
const Vector2f NORMAL_SPEED = Vector2f(150.f,0.f);

//Mother Chicken constants
const int MOTHER_POWERUP_PROB = 701;
const int MOTHER_MEAT_PROB = 710;
const int MOTHER_HEART_PROB = 711;
const int MOTHER_COINS_PROB = 720;
const int MOTHER_IMMUNITY_PROB = 721;
const int MOTHER_EGG_PROB = 700;
const int MOTHER_LP = 20000;
const int MOTHER_VAL = 4000;
const int MOTHER_SIZE = 30;
const Vector2f MOTHER_SPEED = NORMAL_SPEED;

//Trooper chicken constants
const int TROOPER_POWERUP_PROB = 5;
const int TROOPER_MEAT_PROB = 50;
const int TROOPER_HEART_PROB = 7;
const int TROOPER_COINS_PROB = 20;
const int TROOPER_IMMUNITY_PROB = 25;
const int TROOPER_EGG_PROB = 5;
const int TROOPER_LP = 300;
const int TROOPERL_VAL = 60;
const int TROOPER_SIZE = 30;
const Vector2f TROOPER_SPEED = Vector2f(150.f, 0.f);

//Rocket constants
const int ROCKET_LIFE_POINTS = 50;
const Vector2f ROCKET_SPEED = Vector2f(600, 600);

//Damages constants
const int CHICKEN_DMG = ROCKET_LIFE_POINTS;
const int ROCK_DMG = ROCKET_LIFE_POINTS;
const int EGG_DMG = ROCKET_LIFE_POINTS;

//Bullet constants
const int BULLET_LIFE_POINTS = 10;
const int BULLET_DAMAGE = 50;
const int BULLET_DAMAGE_HIGH = 100;

//Weapon constants
const int MIN_WEAPON_POWER = 0;
const int MAX_WEAPON_POWER = 4;
const float BULLET_YGAIN = 400.f;
const float BULLET_DEVIATION_VEL = 20.f;
const float RAD_TO_DEG = float(57.29577951);
const float BULLET_DEVIATION_ROT = float(0.1428571429);

//enum types
enum Wtype {gun, laser, lightning };
enum Rtype {millitary, explorer, alien};
enum Ctype {mother,normal,trooper};
enum Ptype {power, food, money, immune, life};

class object : public Sprite
{
protected:
	int lifepoints; 
	int value;
	int level; 
	int damage; 
	int counter;
	bool active;
	Ptype prizeType;
	Vector2f speed;
	Texture texture;

public:

	object();
	object(int lp, int val, int lvl, int dmg, Vector2f speed, int size, Texture txt);

	void setLP(int lp); //sets lifepoints
	void setVal(int val); //sets value
	void setLvl(int lvl); //sets level
	void setDmg(int dmg); //sets damage
	void setSpeed(Vector2f speed); //sets speed
	void setSize(float sizex, float sizey); //sets size
	void setSize(int squareSize); //rescales texture to be a square of size
	void setGSize(float sizex, float sizey); //sets size on global bounds
	void activate(); //activates object on board
	void deactivate();//deactivates object on board
	void setImg(Texture & txt);//sets texture of sprite
	void setPrizeType(Ptype chosen);


	void levelUp(); //increases the level by one
	void IDamage(); //increases damage one step
	void DDamage(); //decreases damage one step

	void takeDmg(object param); //takes damage from param object if hit

	int getLP() const; //gets Life points
	int getVal() const; //gets value of object
	int getLvl() const; //gets level of object
	int getDmg() const; //gets damage points
	Ptype getPrizeType() const;
	Vector2f getSpeed() const; //gets speed of object
	float getSize() const; //gets size of a square object or width otherwise
	bool isActive() const; //checks if object is avtive
	bool isHit(object param) const; //checks if object is hit (collided) with param
	bool isDead() const; //checks if lifepoints have reached zero
	bool outBounds(FloatRect windowBounds); //checks if outside window
};

#endif // !object_h
