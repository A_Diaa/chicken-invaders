#include "player.h"
#include <iostream>
#include <fstream>
#include <SFML/Graphics.hpp>
#include <string>
#include <istream>
#include "object.h"
using namespace std;
using namespace sf;
player::player()
{
    username = "";
    password = "";
    score = 0;
    coins = 0;
    meat = 0;
	rockets[0] = true;
	weapons[0] = true;
	for (int i = 1; i < 3; i++)
	{
		rockets[i] = false;
		weapons[i] = false;
	}
    //rocketcollections = 0;
}

bool player:: setData(string user,string pass, bool signIn, string& error)
{
    bool success = false;
    if((player::vallidation(user)) && (player::vallidation(pass)))
    {
        if(!signIn)
        {
            if(player::addPlayer(user))
            {
                username = user;
                password = pass;
                score = 0;
                coins = 0;
                meat = 0;

				rockets[0] = true;
				weapons[0] = true;
				for (int i = 1; i < 3; i++)
				{
					rockets[i] = false;
					weapons[i] = false;
				}
                //rocketcollections = 0;
                fstream fout;
#ifdef __APPLE__
                fout.open ("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/database.txt", fstream::in | fstream::out | fstream::app);
#else
				fout.open("database.txt", fstream::in | fstream::out | fstream::app);
#endif
                fout << user << " " << pass << " " << endl;
                fout.close();
                player::updateFile();
                success = true;
            }
            else
                error = "username already taken";
        }
        else
        {
            if(player:: checkCredentials(user,pass))
            {
				username = user; 
				password = pass;
                string filename = username + ".txt";
                ifstream playerfile;
                playerfile.open(filename.c_str());
                if(!playerfile.fail())
                {
                    playerfile >> score >> meat >> coins>>rockets[0]>>rockets[1]>>rockets[2]>>weapons[0]>>weapons[1]>>weapons[2];
                    while(!playerfile.eof())
						playerfile >> score >> meat >> coins >> rockets[0] >> rockets[1] >> rockets[2] >> weapons[0] >> weapons[1] >> weapons[2];
					score = 0;
                    success = true;
                }
                else
                    error = "error: player file failed loading";
            }
            else
                error = "incorrect username or password. Please try again.";
        }
        
    }
    else
        error = "username and password cannot have the space character";
    return success; 
}
void player:: addScore(unsigned int deltascore) //takes unsigned int and adds to score
{
    score += deltascore;
}
void player:: addCoins(int coinsvalue) //takes value (signed)  and adds (or deducts on purchase) from coins
{
    coins += coinsvalue;
}
void player:: addFood(int foodvalue) //takes value (signed)  and adds (or deducts on purchase) from food
{
    meat += foodvalue;
}
void player::updateFile() //player’s file with new score, food, and coins
{
    string filename = username + ".txt";
    fstream out;
    out.open(filename, fstream::in | fstream::out | fstream::app);
    if(!out.fail())
        out << score << " " << meat << " " << coins << " "<< rockets[0]<<" " << rockets[1] << " " << rockets[2] << " " << weapons[0] << " " << weapons[1] << " " << weapons[2]<<endl;
    else
        cout << "problem updating scoree. player file cannot load" << endl;
    out.close();
    
}
bool player:: addPlayer(string user) //checks availability of username only
{
    bool flag = true;
    string x,y; //values for extraction use
    ifstream fin;
#ifdef __APPLE__
	fin.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/database.txt", fstream::in | fstream::out | fstream::app);
#else
	fin.open("database.txt", fstream::in | fstream::out | fstream::app);
#endif
    if(!fin.fail())
    {
        fin >> x >> y;
        while(!fin.eof())
        {
            if(x == user)
                flag = false;
            fin >> x >> y;
        }
        fin.close();
        return flag;
    }
    else
    {    cout << "error loading database file" << endl;
	return false;
    } //after addition,new username and password should be added to the database separated by a space
}
//player::void setRocket(int x); not done here

bool player::setUname(string user) //sets new username for player
{
   if(player::vallidation(user))
   { username = user;
       return true;
   }
   else
        return false;
}
bool player:: setPassword(string pass) //sets password
{
    if(player::vallidation(pass))
    {
        password = pass;
        return true;
    }
    else
        return false;
}
bool player:: checkCredentials(string userName,string passWord) //checks if the player has access rights..username and password should be set first
{
    bool flag = false;
    string user,pass;
    fstream fin;
#ifdef __APPLE__
	fin.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/database.txt", fstream::in | fstream::out | fstream::app);
#else
	fin.open("database.txt", fstream::in | fstream::out | fstream::app);
#endif
    if(!fin.fail())
    {
        fin >> user >> pass;
        while(!fin.eof())
        {
            if((user == userName) && (pass == passWord))
                flag = true;
            fin >> user >> pass;
        }
		if ((user == userName) && (pass == passWord))
			flag = true;
        fin.close();
        return flag;
    }
    else
    {   cout << "error" << endl;
        return 0;
    }
   
}
bool player:: vallidation(string x)
{
    for(int i = 0; i < x.length(); i++)
    {
        if(x[i] == ' ')
           return false;
    }
    return true;
}
int player:: getScore() const
{
    return score;
}
int player:: getMeat() const
{
    return meat;
}
int player:: getCoins() const
{
    return coins;
}
string player:: getUsername() const
{
    return username;
}
string player::getPassword() const
{
    return password;
}
void player :: setScore(int scoreValue)
{
    score = scoreValue;
}

bool player::buyR(int rocketNo)
{
	bool flag = false;
	switch (rocketNo)
	{
	case 1: 
		if (player::getCoins() >= ROCKET1_PRICE)
		{
			flag = true;
			player::addCoins(-ROCKET1_PRICE);
			rockets[0] = true;
		}
		break;
	case 2: 
		if (player::getCoins() >= ROCKET2_PRICE)
		{
			flag = true;
			player::addCoins(-ROCKET2_PRICE);
			rockets[1] = true;
		}
		break;
	case 3: 
		if (player::getCoins() >= ROCKET3_PRICE)
		{
			flag = true;
			player::addCoins(-ROCKET3_PRICE);
			rockets[2] = true;
		}
		break;
	}
	return flag;
}

bool player::buyW(int weaponNo)
{
	bool flag = false;
	switch (weaponNo)
	{
	case 1:
		if (player::getMeat() >= WEAPON1_PRICE)
		{
			flag = true;
			player::addFood(-WEAPON1_PRICE);
			weapons[0] = true;
		}
		break;
	case 2:
		if (player::getMeat() >= WEAPON2_PRICE)
		{
			flag = true;
			player::addFood(-WEAPON2_PRICE);
			weapons[1] = true;
		}
		break;
	case 3:
		if (player::getMeat() >= WEAPON3_PRICE)
		{
			flag = true;
			player::addFood(-WEAPON3_PRICE);
			weapons[2] = true;
		}
		break;
	}
	return flag;
}

bool player::isRocketBought(int rocketNo)
{
	return rockets[rocketNo - 1];
}

bool player::isWeaponBought(int weaponNo)
{
	return weapons[weaponNo - 1];
}
