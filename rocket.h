#pragma once
#ifndef rocket_h
#define rocket_h
#include <iostream>
#include <SFML/Graphics.hpp>
#include "object.h"
#include "bullet.h"
#include <vector>
using namespace std;
using namespace sf;

class rocket : public object
{
private:
    int lives;
    int weaponPower;
	bool immune;
    Wtype weaponType;
	Rtype rocketType;
public:
    rocket();
    bool respawn(); //replenish lifepoints and sets onBoard high.
    void fire(vector <bullet> &); //releases bullets to main boardBullets according to weaponPower and weaponType.
    //requires setting type after firing
	void IWP(); //increases weapon power
    void DWP(); //decreases weapon power
	void minPower(); //puts weapon power to minimum
	void oneUp(); //increases 1 life
    void setWT (Wtype); //assigns weapon to rocket
	void setRT(Rtype, Texture&, Texture&, Texture&); //changes rocket type
	Wtype getWT() const; //gets weapon type
	Rtype getRT() const; //gets rocket type
    void die (); //decrease 1 life, decrease 1 weaponPower, sets onBoard low.
	int getLives() const;
    bool gameOver() const; //checks lives and decides whether game is over
	bool isImmune() const;
	void Immune();
	void deImmune();
	void setLives(int Lives);

};
#endif // !rocket_h
//setters and getters for lives