#ifdef __APPLE__
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/Network.hpp>
#include "ResourcePath.hpp"
#else
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <SFML\Window.hpp>
#include <SFML\Network.hpp>
#endif

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iostream>

#include "back.h"
#include "object.h"
#include "bullet.h"
#include "rocket.h"
#include "chicken.h"
#include "rock.h"
#include "player.h"

using namespace std;
using namespace sf;

//window constants and data
enum Wintype {loading, menu, signUp, logIn, highScores, shop, game, pause, results};

const int WIDTH = 1920, HEIGHT = 1080, FRAME_RATE=30;

const FloatRect wBounds = FloatRect(0.f, 0.f, float(WIDTH), float(HEIGHT)); //window boundaries
const FloatRect dBounds = FloatRect(0.f, 600.f, float(WIDTH), 450.f); //deadboundaries for rocket

<<<<<<< HEAD
RenderWindow window(VideoMode(WIDTH, HEIGHT), "Chicken Invaders" , Style::Fullscreen);
=======

RenderWindow window(VideoMode(WIDTH, HEIGHT), "Chicken Invaders");  //, Style::Fullscreen);

>>>>>>> 416914b11540d331d73da03ef357977ed4986aaa
Wintype state = loading;
int level = 1;
bool signedIn = false;

//logging constants and variables
const float rectW = 50.0, rectL = 300.0;

struct highScore
{
    string name;
    int score;
};

vector <highScore> scoreFile;

//Game objects
player player1, player2;
rocket rocket1, rocket2;
bool dead1=false, dead2=false;
Rtype chosenR = millitary; //by default
Wtype chosenW = gun; //by default
const int CHICK_STEP = 70; //steps for chicken motion in pixels for downChickens
const int IMMUNITY_TIME = 3;

vector <object> eggs;
vector <object> omlettes;
vector <object> pickups;
vector <bullet> bullets;
vector <rock> rocks;
vector <chicken> chickens;

chicken boss; 
rock split;

Music themeM, beginM, logoM, nextM;
Music eggDie,  rockDie ;
Music chickHit, chickLay;
Music fireM, rocketDie, overM, powerM, foodM;

Texture eggT, omletteT; 
Texture powerT, immuneT, heartT, coinT, foodT, bubble1T, bubble2T, bubble3T;
Texture rockT, chicken1T, chicken2T, chicken3T, chicken4T, chicken5T;
Texture bullet1T, bullet2T, bullet3T, rocket1T, rocket2T, rocket3T, shieldT;

object cursor, shield, bubble;

//loading functions
bool loadMusic(); //loads all music files to music variables
bool loadTexture(); //loads all textures to game

//in-game functions
//handlers
void handleOut(); //handles all ACTIVE objects that got out of allowed bounds
void handleCollisions(Clock & immuneC); //handles all importat collisions in the game
void handleInactive(); //deletes all inactive objects in vectors
void handleGame(float); //handles in-game events from the window
void handleDead(Clock &immuneC, Clock & respawnC, float &respawnT);
void handleLevel(); //decides levelStates and death or results calling

//updaters
void updateAll(); //updates chicken and rock animations
void clearAll(); //clears all vectors
void moveAll(float);  //moves all ACTIVE objects on window for next frame
void drawAll(); //draws all ACTIVE objects on window for next frame
void updateBullets(); //updates bullet textures

//game-specific
void releaseYolk(object &); //releases broken egg **needs clearing after 30s
void reverseChickens(); //reverses the direction of all chickens
void downChickens(); // moves the chickens one STEP down

//levels game-functions
void lvl1(); //Normal
void lvl2(); //rocks diagonal TL&DR
void lvl3(); //troopers
void bossF(); //the boss after each level
void pointerslvl1();
void handlePointerlvl(chicken* *p);

//menu functions
void loadingF(); //handles loading window
void menuF(); //handles menu window
void loggingF(bool); //handles login/signup window
void gameF(); //handles game window **branches into other game functions with levels
void resultsF(); //handles results window
void highScoresF(); //handles scores window
void shopF(); //handles shop window
void comingSoon(); //displays alert for upcoming features
void pauseF(); //in-game pause
void bossEntrance(); 
void transitionF(int);
void init();

//for sorting purposes
bool descending (highScore i,highScore j)
{
    return (i.score>j.score);
}

int main()
{
	srand(time(NULL));
	window.setFramerateLimit(FRAME_RATE);
	window.setVerticalSyncEnabled(true);
	window.setMouseCursorVisible(false);
    
   if(loadTexture())
       state = menu;
    
	while (window.isOpen())
	{
		switch (state)
		{

		case loading:
		{
			loadingF();
			break;
		}
		case menu:
		{
            menuF();
			break;
		}
		case signUp:
		{
			loggingF(false);
			break;
		}
		case logIn:
		{
			loggingF(true);
			break;
		}
		case shop:
		{
			if (signedIn)
				shopF();
			else
				comingSoon();
			break;
		}
		case game:
		{
			init();
			gameF();
			break;
		}
		case highScores:
		{
			highScoresF();
			break;
		}
        case results:
        {
            resultsF();
            break;
        }
                

		}

	}
	//system("pause");
	return 0;
}

//moves all vectors according to their speeds
void moveAll(float time)
{
	for (int i = 0; i < eggs.size(); i++)
		if (eggs[i].isActive())
			eggs[i].move(eggs[i].getSpeed()*time);

	for (int i = 0; i < pickups.size(); i++)
		if (pickups[i].isActive())
			pickups[i].move(pickups[i].getSpeed()*time);

	for (int i = 0; i < bullets.size(); i++)
		if (bullets[i].isActive())
		{
			bullets[i].move(-bullets[i].getSpeed()*time);
			if (rocket1.getWT() == lightning)
				bullets[i].setPosition(rocket1.getPosition().x, rocket1.getPosition().y);
		}
			
	for (int i = 0; i < rocks.size(); i++)
		if (rocks[i].isActive())
		{
			rocks[i].move(rocks[i].getSpeed()*time);
		}

	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isActive())
			chickens[i].move(chickens[i].getSpeed()*time);

	if (boss.isActive())
		boss.move(boss.getSpeed()*time);

	if (split.isActive())
		split.move(split.getSpeed()*time);

}

//draws all vectors, clears and displays
void drawAll()
{
	for (int i = 0; i < rocks.size(); i++)
		if (rocks[i].isActive())
			window.draw(rocks[i]);

	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isActive())
			window.draw(chickens[i]);

	for (int i = 0; i < eggs.size(); i++)
		if (eggs[i].isActive())
			window.draw(eggs[i]);

	for (int i = 0; i < pickups.size(); i++)
		if (pickups[i].isActive())
			window.draw(pickups[i]);

	for (int i = 0; i < bullets.size(); i++)
		if (bullets[i].isActive())
			window.draw(bullets[i]);

	for (int i = 0; i < omlettes.size(); i++)
		if (omlettes[i].isActive())
			window.draw(omlettes[i]);

	if (rocket1.isActive())
		window.draw(rocket1);

	if (rocket1.isImmune())
		window.draw(shield);

	if (boss.isActive())
		window.draw(boss);

	if (split.isActive())
		window.draw(split);
}

//deletes all what's outside except chickens; it reverses their speeds
void handleOut() // work in progress
{
	//rocks
	for (int i = 0; i < rocks.size(); i++)
		if (rocks[i].isActive())
		{
			if (rocks[i].getPosition().x<-rocks[i].getGlobalBounds().width || (rocks[i].getPosition().y>HEIGHT+rocks[i].getGlobalBounds().height))
			   rocks[i].deactivate();
		}
	//chickens
	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isActive() && (chickens[i].getGlobalBounds().left<0.f || chickens[i].getGlobalBounds().left+ chickens[i].getGlobalBounds().width>WIDTH))
		{
			if (chickens[i].outBounds(wBounds))
				chickens[i].deactivate();
			reverseChickens();
				break;
		}

	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isActive() && (chickens[i].getGlobalBounds().left<0.f || chickens[i].getGlobalBounds().left + chickens[i].getGlobalBounds().width>WIDTH))
			if (chickens[i].getType() == trooper)
			{
				downChickens();
				break;
			}

	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isActive() && chickens[i].getGlobalBounds().top > HEIGHT && chickens[i].getType() == trooper)
			chickens[i].deactivate();

	if (boss.isActive() && (boss.getGlobalBounds().left < 0.f || boss.getGlobalBounds().left + boss.getGlobalBounds().width >WIDTH))
		boss.setSpeed(Vector2f(-boss.getSpeed().x, boss.getSpeed().y));

	if (boss.isActive() && (boss.getGlobalBounds().top < 0.f || boss.getGlobalBounds().top + boss.getGlobalBounds().height >HEIGHT))
		boss.setSpeed(Vector2f(boss.getSpeed().x, -boss.getSpeed().y));

	if (split.isActive() && (split.getGlobalBounds().left < 0.f || split.getGlobalBounds().left + split.getGlobalBounds().width >WIDTH))
		split.setSpeed(Vector2f(-split.getSpeed().x, split.getSpeed().y));

	if (split.isActive() && (split.getGlobalBounds().top < 0.f || split.getGlobalBounds().top + split.getGlobalBounds().height >HEIGHT))
		split.setSpeed(Vector2f(split.getSpeed().x, -split.getSpeed().y));


	//eggs
	for (int i = 0; i < eggs.size(); i++)
	{
		if (eggs[i].isActive() && eggs[i].getPosition().y >= HEIGHT - eggs[i].getGlobalBounds().height)
		{
			eggs[i].deactivate();
			eggDie.play();
			releaseYolk(eggs[i]);
		}
		else if (eggs[i].isActive() && (eggs[i].getGlobalBounds().left<0.f || eggs[i].getGlobalBounds().left + eggs[i].getGlobalBounds().width>WIDTH))
			eggs[i].setSpeed(Vector2f(-eggs[i].getSpeed().x, eggs[i].getSpeed().y));

	}
	//pickups
	for (int i = 0; i < pickups.size(); i++)
	{
		if (pickups[i].getPrizeType() == food && pickups[i].isActive() && (pickups[i].getGlobalBounds().left<0.f|| pickups[i].getGlobalBounds().left+ pickups[i].getGlobalBounds().width>WIDTH))
		{
			pickups[i].setSpeed(Vector2f(-pickups[i].getSpeed().x, pickups[i].getSpeed().y));
		}

		else if (pickups[i].isActive() && pickups[i].outBounds(wBounds))
			pickups[i].deactivate();
	}

	//bullets
	for (int i = 0; i < bullets.size(); i++)
		if (bullets[i].isActive() && bullets[i].outBounds(wBounds))
			bullets[i].deactivate();
}

//loads texture images
bool loadTexture()
{
	bool flag = true;
#ifdef __APPLE__
    if (!eggT.loadFromFile(resourcePath()+"egg1.png"))
    {
        cout << "eggT not loading" << endl;
        flag = false;
    }
    if (!omletteT.loadFromFile(resourcePath()+"eggyolk.png"))
    {
        cout << "omletteT not loading" << endl;
        flag = false;
    }
    if (!powerT.loadFromFile(resourcePath()+"prize1.png"))
    {
        cout << "powerT not loading" << endl;
        flag = false;
    }
    if (!immuneT.loadFromFile(resourcePath()+"immunity.png"))
    {
        cout << "immuneT not loading" << endl;
        flag = false;
    }
    if (!heartT.loadFromFile(resourcePath()+"heart.png"))
    {
        cout << "heartT not loading" << endl;
        flag = false;
    }
    if (!coinT.loadFromFile(resourcePath()+"coin2.png"))
    {
        cout << "coinT not loading" << endl;
        flag = false;
    }
    if (!foodT.loadFromFile(resourcePath()+"meat2.png"))
    {
        cout << "meatT not loading" << endl;
        flag = false;
    }
    if (!rockT.loadFromFile(resourcePath()+"rock2.png"))
    {
        cout << "rockT not loading" << endl;
        flag = false;
    }
    if (!chicken1T.loadFromFile(resourcePath()+"red1.png"))
    {
        cout << "normal1T not loading" << endl;
        flag = false;
    }
    if (!chicken2T.loadFromFile(resourcePath()+"red2.png"))
    {
        cout << "normal2T not loading" << endl;
        flag = false;
    }
    if (!chicken3T.loadFromFile(resourcePath()+"boss.png"))
    {
        cout << "motherT not loading" << endl;
        flag = false;
    }
    if (!chicken4T.loadFromFile(resourcePath()+"orange1.png"))
    {
        cout << "trooperT not loading" << endl;
        flag = false;
    }
    if (!chicken5T.loadFromFile(resourcePath()+"orange2.png"))
    {
        cout << "trooperT not loading" << endl;
        flag = false;
    }
    if (!bullet1T.loadFromFile(resourcePath()+"bullet1.png"))
    {
        cout << "bullet1T not loading" << endl;
        flag = false;
    }
    if (!bullet2T.loadFromFile(resourcePath()+"bullet2.png"))
    {
        cout << "bullet2T not loading" << endl;
        flag = false;
    }
    if (!bullet3T.loadFromFile(resourcePath()+"bullet3.png"))
    {
        cout << "bullet3T not loading" << endl;
        flag = false;
    }
    if (!rocket1T.loadFromFile(resourcePath()+"rocket1.png"))
    {
        cout << "rocket1T not loading" << endl;
        flag = false;
    }
    if (!rocket2T.loadFromFile(resourcePath()+"rocket2.png"))
    { 
        cout << "rocket2T not loading" << endl;
        flag = false;
    }
    if (!rocket3T.loadFromFile(resourcePath()+"rocket3.png"))
    { 
        cout << "rocket3T not loading" << endl;
        flag = false;
    }
    if (!shieldT.loadFromFile(resourcePath()+"shield.png"))
    {
        cout << "shieldT not loading" << endl;
        flag = false;
    }

   
#else
	if (!eggT.loadFromFile("egg1.png"))
	{
		cout << "eggT not loading" << endl;
		flag = false;
	}
	if (!bubble1T.loadFromFile("bubble1.png"))
	{
		cout << "bubbleT not loading" << endl;
		flag = false;
	}
	if (!bubble2T.loadFromFile("bubble2.png"))
	{
		cout << "bubbleT not loading" << endl;
		flag = false;
	}
	if (!bubble3T.loadFromFile("bubble3.png"))
	{
		cout << "bubbleT not loading" << endl;
		flag = false;
	}

	if (!omletteT.loadFromFile("eggyolk.png"))
	{ 
		cout << "omletteT not loading" << endl;
		flag = false;
	}
	if (!powerT.loadFromFile("prize1.png"))
	{ 
		cout << "powerT not loading" << endl;
		flag = false;
	}
	if (!immuneT.loadFromFile("immunity.png"))
	{ 
		cout << "immuneT not loading" << endl;
		flag = false;
	}
	if (!heartT.loadFromFile("heart.png"))
	{ 
		cout << "heartT not loading" << endl;
		flag = false;
	}
	if (!coinT.loadFromFile("coin2.png"))
	{ 
		cout << "coinT not loading" << endl;
		flag = false;
	}
	if (!foodT.loadFromFile("meat2.png"))
	{ 
		cout << "meatT not loading" << endl;
		flag = false;
	}
	if (!rockT.loadFromFile("rock2.png"))
	{ 
		cout << "rockT not loading" << endl;
		flag = false;
	}
	if (!chicken1T.loadFromFile("red1.png"))
	{ 
		cout << "normal1T not loading" << endl;
		flag = false;
	}
	if (!chicken2T.loadFromFile("red2.png"))
	{ 
		cout << "normal2T not loading" << endl;
		flag = false;
	}
	if (!chicken3T.loadFromFile("boss.png"))
	{ 
		cout << "motherT not loading" << endl;
		flag = false;
	}
	if (!chicken4T.loadFromFile("orange1.png"))
	{
		cout << "trooperT not loading" << endl;
		flag = false;
	}
	if (!chicken5T.loadFromFile("orange2.png"))
	{
		cout << "trooperT not loading" << endl;
		flag = false;
	}
	if (!bullet1T.loadFromFile("bullet1.png"))
	{ 
		cout << "bullet1T not loading" << endl;
		flag = false;
	}
	if (!bullet2T.loadFromFile("bullet2.png"))
	{ 
		cout << "bullet2T not loading" << endl;
		flag = false;
	}
	if (!bullet3T.loadFromFile("bullet3.png"))
	{ 
		cout << "bullet3T not loading" << endl;
		flag = false;
	}
	if (!rocket1T.loadFromFile("rocket1.png"))
	{ 
		cout << "rocket1T not loading" << endl;
		flag = false;
	}
	if (!rocket2T.loadFromFile("rocket2.png"))
	{ 
		cout << "rocket2T not loading" << endl;
		flag = false;
	}
	if (!rocket3T.loadFromFile("rocket3.png"))
	{ 
		cout << "rocket3T not loading" << endl;
		flag = false;
	}
	if (!shieldT.loadFromFile("shield.png"))
	{
		cout << "shieldT not loading" << endl;
		flag = false;
	}
#endif
	return flag;
}

//loads music files
bool loadMusic()
{
	bool flag = true;

	if (!fireM.openFromFile("fire.ogx"))
	{
		cout << "fire not loading" << endl;
		flag = false;
	}
	if (!themeM.openFromFile("main.oga"))
	{
		cout << "theme not loading" << endl;
		flag = false;
	}
	if (!beginM.openFromFile("rocketin.oga"))
	{
		cout << "begin not loading" << endl;
		flag = false;
	}
	if (!logoM.openFromFile("logo.oga"))
	{
		cout << "logo not loading" << endl;
		flag = false;
	}
	if (!eggDie.openFromFile("eggground.oga"))
	{
		cout << "eggDie not loading" << endl;
		flag = false;
	}
	if (!rockDie.openFromFile("killrock.oga"))
	{
		cout << "rockDie not loading" << endl;
		flag = false;
	}
	if (!rocketDie.openFromFile("dead.oga"))
	{
		cout << "rocketDie not loading" << endl;
		flag = false;
	}
	if (!chickHit.openFromFile("hitchicken.oga"))
	{
		cout << "chickHit not loading" << endl;
		flag = false;
	}
	if (!chickLay.openFromFile("egg.oga"))
	{
		cout << "chickLay not loading" << endl;
		flag = false;
	}
	if (!powerM.openFromFile("pickup.oga"))
	{
		cout << "power not loading" << endl;
		flag = false;
	}
	if (!overM.openFromFile("gameover.oga"))
	{
		cout << "over not loading" << endl;
		flag = false;
	}
	if (!foodM.openFromFile("foodeat.oga"))
	{
		cout << "food not loading" << endl;
		flag = false;
	}
	if (!nextM.openFromFile("next.oga"))
	{
		cout << "transitionM not loading" << endl;
		flag = false;
	}
	return flag;
}

//releases an omlette when egg hits the floor **needs clearing every some preset time
void releaseYolk(object & egg)
{
	omlettes.push_back(object());
	omlettes[omlettes.size() - 1].setTexture(omletteT);
	omlettes[omlettes.size() - 1].setSize(20.f);
	omlettes[omlettes.size() - 1].setPosition(egg.getPosition());
	omlettes[omlettes.size() - 1].setSpeed(STATIONARY);
	omlettes[omlettes.size() - 1].activate();
}

//loads and gives user feedback for loading textures and music
void loadingF()
{
	while (state == loading && window.isOpen())
	{
		Clock time;
		time.restart();
		float elapsed;
		bool loaded = false;
		Font defaultF; 
#ifdef __APPLE__
        if (!defaultF.loadFromFile(resourcePath() + "sansation.ttf"))
        {
            cout << "error loading font file";
        }
#else
        if (!defaultF.loadFromFile("default.TTF"))
            cout << "error loading font in menu function" << endl;
#endif


		Text loadingT; 
		loadingT.setString("Loading Assets...");
		loadingT.setFont(defaultF);
		loadingT.setFillColor(Color::Black);
		loadingT.setCharacterSize(40);
		loadingT.setOutlineThickness(2);
		loadingT.setOutlineColor(Color::White);
		loadingT.setPosition((WIDTH / 2)-loadingT.getLocalBounds().width/2, (HEIGHT / 2)-loadingT.getLocalBounds().height/2);


		do
		{
			window.clear(Color::Black);
			window.draw(loadingT);
			window.display();

			if (!loaded)
			{
				if (loadMusic() && loadTexture())
					loaded = true;
			}

			elapsed = time.getElapsedTime().asSeconds();
		} while (!loaded||elapsed<2.f);


		loadingT.setString("loading done");
		loadingT.setPosition((WIDTH / 2) - loadingT.getLocalBounds().width/2, (HEIGHT / 2) - loadingT.getLocalBounds().height/2);
		do
		{
			window.clear(Color::Black);
			window.draw(loadingT);
			window.display();
			elapsed = time.getElapsedTime().asSeconds();
		} while (elapsed < 2.5);

		state = menu;
	}
}

//updates chicken and rocks
void updateAll()
{
	for (int i = 0; i < chickens.size(); i++)
	{
		chickens[i].updateChicken(chicken1T, chicken2T, chicken3T, chicken4T, chicken5T);
		if (chickens[i].drop(pickups, eggs, eggT, powerT, immuneT, heartT, coinT, foodT))
			chickLay.play();
	}

	if (boss.isActive())
		boss.updateChicken(chicken1T, chicken2T, chicken3T, chicken4T, chicken5T);

	if (split.isActive())
		split.updateRock();

	for (int i = 0; i < rocks.size(); i++)
	{
		if (rocks[i].isActive())
		{
			rocks[i].updateRock();
		}
	}

}

//clears all vectors
void clearAll()
{
	eggs.clear(); 
	omlettes.clear(); 
	chickens.clear();
	rocks.clear();
	bullets.clear(); 
	pickups.clear();
	boss.deactivate(); 
	split.deactivate();
}

//handles all collisions in the game
void handleCollisions(Clock &immuneC)
{
	//bullets with chickens
	for (int i = 0; i < bullets.size(); i++)
	{
		//exits the loop if bullet deactivates
		for (int j = 0; j < chickens.size() && bullets[i].isActive(); j++) 
			if (chickens[j].isHit(bullets[i]))
			{
				chickens[j].takeDmg(bullets[i]);
				chickHit.play();
				bullets[i].deactivate();
			}
	}

	//boss with bullets
	for (int i = 0; i < bullets.size(); i++)
	{
		if (boss.isActive()&&boss.isHit(bullets[i])&&bullets[i].isActive())
		{
			boss.takeDmg(bullets[i]); 
			chickHit.play(); 
			bullets[i].deactivate();
		}
	}

	//split with bullets
	for (int i = 0; i < bullets.size(); i++)
	{
		if (split.isHit(bullets[i]) && bullets[i].isActive())
		{
			split.takeDmg(bullets[i]);
			bullets[i].deactivate();
		}
	}

	//bullets with rocks
	for (int i = 0; i < bullets.size(); i++)
	{
		//exits the loop if bullet deactivates
		for (int j = 0; j < rocks.size() && bullets[i].isActive(); j++) 
			if (rocks[j].isHit(bullets[i]))
			{
				rocks[j].takeDmg(bullets[i]);
				bullets[i].deactivate();
			}
	}

	//eggs with rocket
	for (int i = 0; i < eggs.size(); i++)
	{
		//Do not let rocket die if it's in immunity or if the egg is inactive
		if (eggs[i].isActive() &&rocket1.isActive()&& rocket1.isHit(eggs[i]) && !rocket1.isImmune())
		{
			rocketDie.play();
			rocket1.die();
			eggs[i].deactivate();
			//if there are remaining lives, respawn and make immunity [initial immunity]

		}
	}

	//chickens with rocket
	for (int i = 0; i < chickens.size(); i++)
	{
		//Do not let rocket die if it's in immunity or if the chickens is inactive
		if (chickens[i].isActive() && rocket1.isActive() && rocket1.isHit(chickens[i]) && !rocket1.isImmune())
		{
			rocketDie.play();
			rocket1.die();
			chickens[i].deactivate();
		}
	}

	if (boss.isActive() && rocket1.isActive() && rocket1.isHit(boss) && !rocket1.isImmune())
	{
		rocketDie.play();
		rocket1.die();
	}

	//Do not let rocket die if it's in immunity or if the rocks is inactive
	if (split.isActive() && rocket1.isHit(split) && rocket1.isActive() && !rocket1.isImmune())
	{
		rocketDie.play();
		rocket1.die();
		split.deactivate();
	}

	//rocks with rocket
	for (int i = 0; i < rocks.size(); i++)
	{
		//Do not let rocket die if it's in immunity or if the rocks is inactive
		if (rocks[i].isActive() && rocket1.isHit(rocks[i]) && rocket1.isActive() && !rocket1.isImmune())
		{
			rocketDie.play();
			rocket1.die();
			rocks[i].deactivate();
		}
	}

	//prizes with rocket
	for (int i = 0; i < pickups.size(); i++)
	{
		if (pickups[i].isHit(rocket1) && rocket1.isActive() && pickups[i].isActive())
		{
			switch (pickups[i].getPrizeType())
			{
			case immune:
				beginM.play();
				immuneC.restart(); 
				rocket1.Immune();
				break;
			case power:
				powerM.play();
				rocket1.IWP();
				break;
			case food:
				foodM.play();
				player1.addFood(MEAT_VAL);
				break;
			case life:
				beginM.play();
				rocket1.oneUp();
				break;
			case money:

				foodM.play();
				player1.addCoins(COIN_VAL);
				break;
			default:
				break;
			}
			pickups[i].deactivate();
		}
	}
}

//handles all inactive objects
void handleInactive()
{
	//rocks
	for (int i = 0; i < rocks.size(); i++)
		if (!rocks[i].isActive())
		{
			rocks.erase(rocks.begin() + i);
			i--;
		}
	//chickens
	for (int i = 0; i < chickens.size(); i++)
		if (!chickens[i].isActive())
		{
			chickens.erase(chickens.begin() + i);
			i--;
		}
	//eggs
	for (int i = 0; i < eggs.size(); i++)
		if (!eggs[i].isActive())
		{
			eggs.erase(eggs.begin() + i);
			i--;
		}
	//pickups
	for (int i = 0; i < pickups.size(); i++)
		if (!pickups[i].isActive())
		{
			player1.addScore(pickups[i].getVal());
			pickups.erase(pickups.begin() + i);
			i--;
		}
	//bullets
	for (int i = 0; i < bullets.size(); i++)
		if (!bullets[i].isActive())
		{
			bullets.erase(bullets.begin() + i);
			i--;
		}
}

//reverses directions of all chickens
void reverseChickens()
{
	for (int i = 0; i < chickens.size(); i++)
		chickens[i].setSpeed(-chickens[i].getSpeed());
}

//moves all chickens one CHICK_STEP down
void downChickens()
{
	for (int i = 0; i < chickens.size(); i++)
		chickens[i].setPosition(chickens[i].getPosition().x, chickens[i].getPosition().y + CHICK_STEP);
}

//displays menu and views different optionss
void menuF()
{
	back back1;
	Clock clock;
	float time;
	Font font;
	themeM.setLoop(false);
	themeM.play();
	init();
	switch (chosenR)
	{
	case millitary: 
		cursor.setTexture(rocket1T);
		cursor.setSize(65);
		cursor.setOrigin(cursor.getLocalBounds().width / 2.f, cursor.getLocalBounds().height / 2.f);
		break; 
	case explorer: 
		cursor.setTexture(rocket2T);
		cursor.setSize(65);
		cursor.setOrigin(cursor.getLocalBounds().width / 2.f, cursor.getLocalBounds().height / 2.f);
		break;
	case alien:
		cursor.setTexture(rocket3T);
		cursor.setSize(65);
		cursor.setOrigin(cursor.getLocalBounds().width / 2.f, cursor.getLocalBounds().height / 2.f);
		break;
	}

#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
	if (!font.loadFromFile("default.TTF"))
		cout << "error loading font in menu function" << endl;
#endif

	Text title;
	title.setFont(font);
	title.setCharacterSize(90.f);
	title.setOutlineThickness(4.5);
	title.setFillColor(Color::Black);
	title.setOutlineColor(Color::White);
	title.setString("CHICKEN INVADERS");
	title.setPosition(WIDTH / 2 - title.getLocalBounds().width / 2.f, 10.f);
	//create items here [start game, highscores, signUp, logIn, Quit, shop]
	RectangleShape buttons[6];
	Text buttonT[6];
	FloatRect buttonB[6];
	buttonT[0].setString("Start game"); 
	buttonT[1].setString("High Scores");
	buttonT[2].setString("Sign Up");
	buttonT[3].setString("Log In");
	buttonT[4].setString("SHOP");
	buttonT[5].setString("QUIT");

	for (int i = -3; i < 3; i++)
	{
		buttons[i + 3].setFillColor(Color::Black);
		buttons[i + 3].setSize(Vector2f(500.f, 100.f));
		buttons[i + 3].setPosition(Vector2f(float(WIDTH) / 2.f -250.f, float(HEIGHT) / 2.f + i*150.f+100.f));

		buttonT[i + 3].setFont(font); 
		buttonT[i + 3].setFillColor(Color::Black);
		buttonT[i + 3].setCharacterSize(40.f);
		buttonT[i + 3].setOutlineThickness(2.f);
		buttonT[i + 3].setOutlineColor(Color::White);
		buttonT[i + 3].setPosition(buttons[i + 3].getPosition().x+buttons[i+3].getLocalBounds().width/2-buttonT[i+3].getLocalBounds().width/2, buttons[i + 3].getPosition().y + buttons[i + 3].getLocalBounds().height / 2 - buttonT[i + 3].getLocalBounds().height / 2);
		buttonB[i + 3] = buttons[i + 3].getGlobalBounds();
	}

	while (window.isOpen() && state == menu)
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		Event event; 
		while (window.pollEvent(event))
		{
//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed: 
				window.close();
				break;
//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					window.close();
				}
				break;
//MOUSE EVENTS HERE
			case Event::MouseButtonPressed:
				if (event.mouseButton.button==Mouse::Left)
					for (int i = 0; i < 6; i++)
					{
						if (buttonB[i].contains(event.mouseButton.x, event.mouseButton.y))
						{
							buttons[i].setFillColor(Color::Blue);
							switch (i)
							{
							case 0:
								state = game;
								themeM.stop();
								//gameF();
								break;
							case 1:
								state = highScores;
								break;
							case 2:
								state = signUp;
								break;
							case 3:
								state = logIn;
								break;
							case 4:
								state = shop;
								break;
							case 5:
								window.close();
								break;
							}
							break;
						}
						else

						{
							buttons[i].setFillColor(Color::Black);
							continue;
						}
					}
				break;
			case Event::MouseMoved:
				cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
				break;

			}
		}


		window.clear();
		back1.spriteUp(time);
		back1.drawOn(window);
		window.draw(title);
		for (int i = 0; i < 6; i++)
		{
			window.draw(buttons[i]);
			window.draw(buttonT[i]);
		}
		window.draw(cursor);
		window.display();

	}
}

void loggingF(bool signIn)
{
	string nameText, passText, asterisk;
	Vector2i mousepos;
	bool flagU = false;
	bool flagP = false;
	bool success = false;
	back back1;
	Clock clock;
	float time;

	//username text
	Font font;
#ifdef __APPLE__
	if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
	{
		cout << "error loading font file";
	}
#else
	if (!font.loadFromFile("default.TTF"))
	{
		cout << "error loading font file";
	}
#endif
	//etner button
	RectangleShape button(Vector2f(120.0, 75.0));
	button.setFillColor(Color::Black);

	Text textB("okay", font, 40);

	Text text("username", font, 40);
	text.setFillColor(Color::Black);
	text.setOutlineColor(Color::White);
	text.setOutlineThickness(2.f);

	//password text
	Text textP("password", font, 40);
	textP.setFillColor(Color::Black);
	textP.setOutlineColor(Color::White);
	textP.setOutlineThickness(2.f);

	//username rect
	RectangleShape rect(Vector2f(rectL, rectW));
	rect.setFillColor(Color::White);

	//password rect
	RectangleShape rectP(Vector2f(rectL, rectW));
	rectP.setFillColor(Color::White);

	//back button
	RectangleShape backB(Vector2f(200.f, 100.f));
	backB.setFillColor(Color::Black);

	Text backText("BACK", font, 40);
	backText.setFillColor(Color::Black);
	backText.setOutlineColor(Color::White);
	backText.setOutlineThickness(2.f);

	//error text
	Text errorText("", font, 40);
	errorText.setFillColor(Color::Black);
	errorText.setOutlineColor(Color::White);
	errorText.setOutlineThickness(2.f);
	string error;

	//positions
	errorText.setPosition(WIDTH / 2.f - errorText.getLocalBounds().width / 2.f, 100.f);
	backB.setPosition(WIDTH - backB.getLocalBounds().width, HEIGHT - backB.getLocalBounds().height);
	backText.setPosition(backB.getGlobalBounds().left+backB.getGlobalBounds().width/2.f-backText.getLocalBounds().width/2.f, backB.getGlobalBounds().top + backB.getGlobalBounds().height / 2.f - backText.getLocalBounds().height / 2.f);
	
	rectP.setPosition(WIDTH/2.f-rectP.getLocalBounds().width/2.f, HEIGHT / 2.f - rectP.getLocalBounds().height / 2.f);
	rect.setPosition(rectP.getGlobalBounds().left, rectP.getGlobalBounds().top-rect.getLocalBounds().height-20.f);
	button.setPosition(WIDTH/2.f-button.getLocalBounds().width/2.f, rectP.getGlobalBounds().top + rectP.getLocalBounds().height + 20.f);
	
	
	textP.setPosition(rectP.getPosition());
	text.setPosition(rect.getPosition());
	textB.setPosition(button.getGlobalBounds().left+button.getGlobalBounds().width/2.f-textB.getLocalBounds().width/2.f, button.getGlobalBounds().top + button.getGlobalBounds().height / 2.f - textB.getLocalBounds().height / 2.f);


	while ((state == logIn || state == signUp) && (window.isOpen()) && (!success))
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();
		Event Event;
		while (window.pollEvent(Event))
		{
			if (Event.type == Event::MouseMoved)
			{
				cursor.setPosition(Event.mouseMove.x, Event.mouseMove.y);
			}
			if (Event.type == Event::Closed)
				window.close();

			if (Event.type == Event::MouseButtonPressed)
			{
				if (Event.mouseButton.button == Mouse::Left)
				{
					mousepos = Mouse::getPosition(window);
					if (rect.getGlobalBounds().contains(mousepos.x, mousepos.y))
					{
						flagU = true;
						text.setString("");
						flagP = false;

					}
					if (rectP.getGlobalBounds().contains(mousepos.x, mousepos.y))
					{
						flagP = true;
						textP.setString("");
						flagU = false;

					}
					if (button.getGlobalBounds().contains(mousepos.x, mousepos.y))
					{
                        for(int i=0; i< nameText.length();i++)
                        {
                            nameText[i] = char(tolower(nameText[i]));
                        }
                        success = player1.setData(nameText, passText, signIn, error);
						text.setString("username");
						textP.setString("password");
						nameText = "";
						passText = "";
						asterisk = "";
					}
					if (backB.getGlobalBounds().contains(mousepos.x, mousepos.y))
					{
						state = menu;
					}

				}

			}
            if (flagP)

            {
                   
				if (Event.type == Event::TextEntered && passText.length() < 16)
					if ( (Event.text.unicode != 8) && (Event.text.unicode != 10))
                    {
                        if( (Event.text.unicode >= 48 && Event.text.unicode <= 57) || (Event.text.unicode >= 65 && Event.text.unicode <= 90) || (Event.text.unicode >= 97 && Event.text.unicode <= 122) || (Event.text.unicode == 46) || (Event.text.unicode == 95) || (Event.text.unicode == 32))
					    {
						  passText += (char)(Event.text.unicode);
						  asterisk += '*';
						  textP.setString(asterisk);
                        }
                    }
				if (Event.type == Event::KeyPressed)
					if ((Event.key.code == Keyboard::BackSpace) && (passText.length() > 0))
					{
						passText.erase(passText.length() - 1, 1);
						asterisk.erase(asterisk.length() - 1, 1);
						textP.setString(asterisk);
					}
			   }
           
			else if(flagU)
			{
				if (Event.type == Event::TextEntered && (nameText.length() < 12))
				{
                    
                    if ( (Event.text.unicode != 8) && (Event.text.unicode != 10))
                    {
                        if( (Event.text.unicode >= 48 && Event.text.unicode <= 57) || (Event.text.unicode >= 65 && Event.text.unicode <= 90) || (Event.text.unicode >= 97 && Event.text.unicode <= 122) || (Event.text.unicode == 46) || (Event.text.unicode == 95) || (Event.text.unicode == 32))
					{
						cout << (char)Event.text.unicode << endl;
						nameText += (char)(Event.text.unicode);
						text.setString(nameText);
					}
                    }
				}
				if (Event.type == Event::KeyPressed)
					if ((Event.key.code == Keyboard::BackSpace) && (nameText.length() > 0))
					{
						nameText.erase(nameText.length() - 1, 1);
						text.setString(nameText);
					}
			}

			if (Event.type == Event::KeyPressed)
				if (Event.key.code == Keyboard::Return)
				{
					success = player1.setData(nameText, passText, signIn, error);
					text.setString("username");
					textP.setString("password");
					nameText = "";
					passText = "";
					asterisk = "";
				}
		}
		if ((!success) && (signIn))
		{
			errorText.setString(error);
			errorText.setPosition(WIDTH / 2.f - errorText.getGlobalBounds().width / 2.f, 100.f);
		}
		else
			if ((!success) && (!signIn))
			{
				errorText.setString(error);
				errorText.setPosition(WIDTH / 2.f - errorText.getGlobalBounds().width / 2.f, 100.f);
			}

		if (success)
		{
			signedIn = true;
			state = menu;
		}
		back1.spriteUp(time);
		window.clear();
		back1.drawOn(window);
		window.draw(rect);
		window.draw(rectP);
		window.draw(button);
		window.draw(backB);
		window.draw(text);
		window.draw(textP);
		window.draw(textB);
		window.draw(backText);
		window.draw(errorText);
		window.draw(cursor);
		window.display();
	}
}

void resultsF()
{
    //files part
    player1.updateFile();
    fstream fout;
    ifstream fin;
    string tempN;
    int tempS;
    int lastScore = player1.getScore();
    int maxScore = 0; // in case last score is not max
    bool topWorld = true;
    bool topPersonal = true;

    //after it's 100% working , i will make it a function in the player class
    fstream playerfile;
#ifdef __APPLE__
    playerfile.open(player1.getUsername() + ".txt", fstream::in | fstream::out | fstream::app);
#else
    playerfile.open(player1.getUsername() + ".txt", fstream::in | fstream::out | fstream::app);
#endif
    int tempSc,tempM,tempC;
    playerfile >> tempSc >> tempM >> tempC;
    while(!playerfile.eof())
    {
        if(tempSc > lastScore)
        {
            topPersonal = false;
            maxScore = tempSc;
        }
         playerfile >> tempSc >> tempM >> tempC;
    }
    playerfile.close();



    
#ifdef __APPLE__
    fout.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/highscores.txt", fstream::in | fstream::out | fstream::app);
#else
    fout.open("highscores.txt", fstream::in | fstream::out | fstream::app);
#endif
    if(player1.getUsername()=="")
		fout << "Guest"<<to_string(rand()%10000) << " "<< player1.getScore() << endl;
	else
		fout << player1.getUsername() <<" " << player1.getScore() << endl;
    fout.close();
    
    player1.setScore(0);

    
#ifdef __APPLE__
    fin.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/highscores.txt", fstream::in | fstream::out | fstream::app);
#else
    fin.open("highscores.txt", fstream::in | fstream::out | fstream::app);
#endif
    
    fin >> tempN >> tempS;
    for(int i=0;(!fin.eof()); i++)
    {
        if(tempS > lastScore)
            topWorld = false;
        scoreFile.push_back(highScore());
        scoreFile[i].name = tempN;
        scoreFile[i].score = tempS;
        fin >> tempN >> tempS;
    }
    fin.close();
    

	sort(scoreFile.begin(), scoreFile.end(), descending);
	if (scoreFile.size()>10)
		scoreFile.erase(scoreFile.begin() + 10, scoreFile.end());

    
    ofstream final;
#ifdef __APPLE__
    final.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/highscores.txt");
#else
    final.open("highscores.txt");
#endif
    for(int i=0; i< scoreFile.size(); i++)
    {
        final << scoreFile[i].name << " "<< scoreFile[i].score << endl;
    }
    final.close();
    
    
   //display part
    RectangleShape table;
    table.setFillColor(Color::Black);
    table.setPosition(300.f,300.f);
    table.setSize(Vector2f(1320.f,580.f));
    Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif


    Text highText[2][2];
    Text topWorldText;
    Text topPersonalText;
	Text currentP;

	currentP.setFont(font);
	currentP.setCharacterSize(40.f);
	currentP.setOutlineThickness(2.f);
	currentP.setFillColor(Color::Black);
	currentP.setOutlineColor(Color::White);
	currentP.setPosition(320.f, 320.f);
	if (player1.getUsername()=="")
		currentP.setString("Guest player");
	else
	currentP.setString(player1.getUsername()); 

    for(int i=0; i<2;i++)
        for (int j=0; j<2; j++)
        {
            highText[i][j].setFont(font);
            highText[i][j].setCharacterSize(40.f);
            highText[i][j].setOutlineThickness(2.f);
            highText[i][j].setFillColor(Color::Black);
            highText[i][j].setOutlineColor(Color::White);
            highText[i][j].setPosition(320 + j * 640,320+i*54+100);
        }
    
    highText[0][0].setString("You scored:");
    highText[0][1].setString(to_string(lastScore));
    
    highText[1][0].setString("Your Highest Score:");
    if(topPersonal)
      highText[1][1].setString(to_string(lastScore));
    else
      highText[1][1].setString(to_string(maxScore));
    
    topWorldText.setFont(font);
    topWorldText.setCharacterSize(40.f);
    topWorldText.setOutlineThickness(2.f);
    topWorldText.setFillColor(Color::Black);
    topWorldText.setOutlineColor(Color::White);
	topWorldText.setString("New highscore!");
    topWorldText.setPosition(WIDTH/2.f -topWorldText.getGlobalBounds().width/2.f,table.getGlobalBounds().top + 400.f);

    
    topPersonalText.setFont(font);
    topPersonalText.setCharacterSize(40.f);
    topPersonalText.setOutlineThickness(2.f);
    topPersonalText.setFillColor(Color::Black);
    topPersonalText.setOutlineColor(Color::White);
	topPersonalText.setString("New personal best!");
    topPersonalText.setPosition(table.getGlobalBounds().left + table.getGlobalBounds().width/2.f - topPersonalText.getGlobalBounds().width/2.f ,table.getGlobalBounds().top + 450.f);

    
    
    back back1;
    Clock clock;
    float time;
    
    
    
    RectangleShape okayB;
    okayB.setFillColor(Color::Black);
    okayB.setSize(Vector2f(200.f, 100.f));
    okayB.setPosition(WIDTH - okayB.getGlobalBounds().width, HEIGHT - okayB.getGlobalBounds().height);
    
    Text okayT;
    okayT.setFont(font);
    okayT.setCharacterSize(40.f);
    okayT.setOutlineThickness(2.f);
    okayT.setFillColor(Color::Black);
    okayT.setOutlineColor(Color::White);
    okayT.setString("OKAY");
    okayT.setPosition(WIDTH - okayB.getGlobalBounds().width / 2 - okayT.getLocalBounds().width / 2, HEIGHT - okayB.getGlobalBounds().height / 2 - okayT.getLocalBounds().height / 2);
    
    Text info;
    info.setFont(font);
    info.setCharacterSize(40.f);
    info.setOutlineThickness(2.f);
    info.setFillColor(Color::Black);
    info.setOutlineColor(Color::White);
    info.setString("RESULTS");
    info.setPosition(WIDTH / 2.f - info.getLocalBounds().width / 2.f, 100.f);
    
    
    
    while (window.isOpen()&&state== results)
    {
        time = clock.getElapsedTime().asSeconds();
        clock.restart();
        
        Event event;
        while (window.pollEvent(event))
        {
            //WINDOW EVENTS HERE
            switch (event.type)
            {
                case Event::Closed:
                    window.close();
                    break;
                    //KEYBOARD EVENTS HERE
                case Event::KeyPressed:
                    switch (event.key.code)
                {
                    case Keyboard::Escape:
                        window.close();
                }
                    break;
                    //MOUSE EVENTS HERE
                case Event::MouseButtonPressed:
                    if (event.mouseButton.button == Mouse::Left)
                    {
                        
                        if (okayB.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            okayB.setFillColor(Color::Blue);
                            state = highScores;
							dead1 = false;
							level = 1;
                        }
                    }
                    break;
                case Event::MouseMoved:
                    cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
                    break;
                    
            }
        }

        
        back1.spriteUp(time);
        
        window.clear();
        back1.drawOn(window);
        window.draw(table);
        window.draw(info);
        for(int i=0; i<2;i++)
            for (int j=0; j<2; j++)
                window.draw(highText[i][j]);
        if(topWorld)
            window.draw(topWorldText);
        if(topPersonal)
            window.draw(topPersonalText);
        window.draw(okayB);
        window.draw(okayT);
		window.draw(currentP);
        window.draw(cursor);
        
        window.display();
    }
    scoreFile.clear();
	dead1 = false;
}

void highScoresF()
{
    ifstream fin;
    string tempN;
    int tempS;
#ifdef __APPLE__
    fin.open("/Users/mariamhegazy/Desktop/chicken invaders/chicken-invaders/highscores.txt");// , fstream::in | fstream::out | fstream::app);
#else
    fin.open("highscores.txt"); //, fstream::in | fstream::out | fstream::app);
#endif
    RectangleShape table;
    table.setFillColor(Color::Black);
    table.setPosition(300.f,300.f);
    table.setSize(Vector2f(1320.f,580.f));

    fin >> tempN >> tempS;
    for(int i=0; (i< 10)&&(!fin.eof()); i++)
    {
        scoreFile.push_back(highScore());
        scoreFile[i].name = tempN;
        scoreFile[i].score = tempS;
        fin >> tempN >> tempS;
    }
    fin.close();

    
    Text highText [10][2];
    Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif

    for(int i=0; i<10;i++)
        for (int j=0; j<2; j++)
        {
			highText[i][j].setFont(font);
            highText[i][j].setCharacterSize(40.f);
            highText[i][j].setOutlineThickness(2.f);
            highText[i][j].setFillColor(Color::Black);
            highText[i][j].setOutlineColor(Color::White);
            highText[i][j].setPosition(320 + j * 640,320+i*54);
        }
    
    for (int i=0; i<scoreFile.size(); i++)
    {
        highText[i][0].setString(scoreFile[i].name);
        highText[i][1].setString(to_string(scoreFile[i].score));
    }
    
    back back1;
    Clock clock;
    float time;


    
    RectangleShape okayB;
    okayB.setFillColor(Color::Black);
    okayB.setSize(Vector2f(200.f, 100.f));
    okayB.setPosition(WIDTH - okayB.getGlobalBounds().width, HEIGHT - okayB.getGlobalBounds().height);
    
    Text okayT;
    okayT.setFont(font);
    okayT.setCharacterSize(40.f);
    okayT.setOutlineThickness(2.f);
    okayT.setFillColor(Color::Black);
    okayT.setOutlineColor(Color::White);
    okayT.setString("OKAY");
    okayT.setPosition(WIDTH - okayB.getGlobalBounds().width / 2 - okayT.getLocalBounds().width / 2, HEIGHT - okayB.getGlobalBounds().height / 2 - okayT.getLocalBounds().height / 2);
    
    Text info;
    info.setFont(font);
    info.setCharacterSize(40.f);
    info.setOutlineThickness(2.f);
    info.setFillColor(Color::Black);
    info.setOutlineColor(Color::White);
    info.setString("HIGHSCORES");
    info.setPosition(WIDTH / 2.f - info.getLocalBounds().width / 2.f, 100.f);
    
    
    
    while (window.isOpen()&&state==highScores)
    {
        time = clock.getElapsedTime().asSeconds();
        clock.restart();
        
        Event event;
        while (window.pollEvent(event))
        {
            //WINDOW EVENTS HERE
            switch (event.type)
            {
                case Event::Closed:
                    window.close();
                    break;
                    //KEYBOARD EVENTS HERE
                case Event::KeyPressed:
                    switch (event.key.code)
                {
                    case Keyboard::Escape:
                        window.close();
                }
                    break;
                    //MOUSE EVENTS HERE
                case Event::MouseButtonPressed:
                    if (event.mouseButton.button == Mouse::Left)
                    {
                        
                        if (okayB.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                        {
                            okayB.setFillColor(Color::Blue);
                            state = menu;
                        }
                    }
                    break;
                case Event::MouseMoved:
                    cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
                    break;
                    
            }
        }
        
        
        back1.spriteUp(time);
        
        window.clear();
        back1.drawOn(window);
        window.draw(table);
        for(int i=0; i<10;i++)
            for (int j=0; j<2; j++)
                window.draw(highText[i][j]);
        window.draw(info);
        window.draw(okayB);
        window.draw(okayT);
        window.draw(cursor);

        window.display();
    }
	scoreFile.clear();
}

//displays alert for upcoming features
void comingSoon()
{
	back back1;
	Clock clock;
	float time;
	Font font;
	if (!font.loadFromFile("default.TTF"))
		cout << "error loading font in menu function" << endl;

	bool flag = true; //as long as okay button is not pressed

	RectangleShape okayB;
	okayB.setFillColor(Color::Black);
	okayB.setSize(Vector2f(200.f, 100.f));
	okayB.setPosition(WIDTH - okayB.getGlobalBounds().width, HEIGHT - okayB.getGlobalBounds().height);

	Text okayT;
	okayT.setFont(font);
	okayT.setCharacterSize(40.f);
	okayT.setOutlineThickness(2.f);
	okayT.setFillColor(Color::Black);
	okayT.setOutlineColor(Color::White);
	okayT.setString("OKAY");
	okayT.setPosition(WIDTH - okayB.getGlobalBounds().width / 2 - okayT.getLocalBounds().width / 2, HEIGHT - okayB.getGlobalBounds().height / 2 - okayT.getLocalBounds().height / 2);

	Text info;
	info.setFont(font);
	info.setCharacterSize(40.f);
	info.setOutlineThickness(2.f);
	info.setFillColor(Color::Black);
	info.setOutlineColor(Color::White);
	info.setString("You have to be logged in to access the shop");
	info.setPosition(WIDTH / 2.f - info.getLocalBounds().width / 2.f, HEIGHT/2.f-info.getLocalBounds().height/2.f);



	while (window.isOpen()&&flag)
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		Event event;
		while (window.pollEvent(event))
		{
			//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed:
				window.close();
				break;
				//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					window.close();
				}
				break;
				//MOUSE EVENTS HERE
			case Event::MouseButtonPressed:
				if (event.mouseButton.button == Mouse::Left)
				{
					
					if (okayB.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
					{
						okayB.setFillColor(Color::Blue);
						state = menu;
						menuF();
						flag = false;
					}
				}
				break;
			case Event::MouseMoved:
				cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
				break;

			}
		}

	
		back1.spriteUp(time);

		window.clear();
		back1.drawOn(window);
		window.draw(info);
		window.draw(okayB);
		window.draw(okayT);
		window.draw(cursor);
		window.display();
	}

}

void shopF()
{
	back back1;
	Clock clock;
	float time;
	Font font;
<<<<<<< HEAD
	string rocketT, weaponT, currentFS, currentMS;
	Text currentF, currentM, error;
	currentFS = "Current Food: " + to_string(player1.getMeat());
	currentMS= "Current Coins: " + to_string(player1.getCoins());
	if (!font.loadFromFile("default.TTF"))
		cout << "error loading font in menu function" << endl;
=======
	string rocketT, weaponT;
    
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif
>>>>>>> 416914b11540d331d73da03ef357977ed4986aaa

	RectangleShape okayB;
	okayB.setFillColor(Color::Black);
	okayB.setSize(Vector2f(200.f, 100.f));
	okayB.setPosition(WIDTH - okayB.getGlobalBounds().width, HEIGHT - okayB.getGlobalBounds().height);

	error.setFont(font);
	error.setCharacterSize(25.f);
	error.setOutlineThickness(1.f);
	error.setFillColor(Color::Black);
	error.setOutlineColor(Color::White);
	error.setPosition(1280.f, 600.f);

	currentF.setFont(font);
	currentF.setCharacterSize(40.f);
	currentF.setOutlineThickness(2.f);
	currentF.setFillColor(Color::Black);
	currentF.setOutlineColor(Color::White);
	currentF.setString(currentFS);
	currentF.setPosition(1280.f, 300.f);

	currentM.setFont(font);
	currentM.setCharacterSize(40.f);
	currentM.setOutlineThickness(2.f);
	currentM.setFillColor(Color::Black);
	currentM.setOutlineColor(Color::White);
	currentM.setString(currentMS);
	currentM.setPosition(1280.f, 400.f);
	
	Text okayT;
	okayT.setFont(font);
	okayT.setCharacterSize(40.f);
	okayT.setOutlineThickness(2.f);
	okayT.setFillColor(Color::Black);
	okayT.setOutlineColor(Color::White);
	okayT.setString("OKAY");
	okayT.setPosition(WIDTH - okayB.getGlobalBounds().width/2-okayT.getLocalBounds().width/2, HEIGHT - okayB.getGlobalBounds().height/2-okayT.getLocalBounds().height/2);

	Text info; 
	info.setFont(font);
	info.setCharacterSize(40.f); 
	info.setOutlineThickness(2.f); 
	info.setFillColor(Color::Black); 
	info.setOutlineColor(Color::White); 
	info.setString("Choose your loadout to save the world"); 
	info.setPosition(WIDTH / 2 - info.getLocalBounds().width / 2.f, 0.f);

	Text rocketNow; 
	rocketNow.setFont(font);
	rocketNow.setCharacterSize(25.f);
	rocketNow.setOutlineThickness(2.f);
	rocketNow.setFillColor(Color::Black);
	rocketNow.setOutlineColor(Color::White);
	rocketNow.setString("Current Equipped Rocket: "+rocketT);
	rocketNow.setPosition(1280.f, 800.f);

	Text weaponNow; 
	weaponNow.setFont(font);
	weaponNow.setCharacterSize(25.f);
	weaponNow.setOutlineThickness(2.f);
	weaponNow.setFillColor(Color::Black);
	weaponNow.setOutlineColor(Color::White);
	weaponNow.setString("Current Equipped Weapon: " + weaponT);
	weaponNow.setPosition(1280.f, 900.f);

	object ships[3]; 
	object weapons[3];
	Text prices[3], shipS[3], weaponS[3]; 
	FloatRect shipB[3], weaponB[3];

	for (int i = 0; i < 3; i++)
	{
		prices[i].setFont(font);
		prices[i].setCharacterSize(40.f);
		prices[i].setOutlineThickness(2.f);
		prices[i].setFillColor(Color::Black);
		prices[i].setOutlineColor(Color::White);
		prices[i].setString(to_string((i+1)*10000));
		prices[i].setPosition(i * 384 + 150, 60.f);

		shipS[i].setFont(font);
		shipS[i].setCharacterSize(40.f);
		shipS[i].setOutlineThickness(2.f);
		shipS[i].setFillColor(Color::Black);
		shipS[i].setOutlineColor(Color::White);
		if  (player1.isRocketBought(i+1))
			shipS[i].setString("OWNED");
		else
			shipS[i].setString("BUY NOW!");
		shipS[i].setPosition(i * 384 + 150, 110.f);

		weaponS[i].setFont(font);
		weaponS[i].setCharacterSize(40.f);
		weaponS[i].setOutlineThickness(2.f);
		weaponS[i].setFillColor(Color::Black);
		weaponS[i].setOutlineColor(Color::White);
		if (player1.isWeaponBought(i + 1))
			weaponS[i].setString("OWNED");
		else
			weaponS[i].setString("BUY NOW!");
		weaponS[i].setPosition(i * 384 + 150, 950.f);
	}
	ships[0].setTexture(rocket1T);
	ships[1].setTexture(rocket2T);
	ships[2].setTexture(rocket3T);
	weapons[0].setTexture(bullet1T);
	weapons[1].setTexture(bullet2T);
	weapons[2].setTexture(bullet3T);

	for (int i = 0; i < 3; i++)
	{
		ships[i].setSize(480); 
		ships[i].setPosition(i * 384.f, 200.f);
		//ships[i].setColor(Color::Red);
		shipB[i] = ships[i].getGlobalBounds();

		weapons[i].setSize(100.f, 150.f);
		weapons[i].setPosition(i * 384+270, 900.f);
		weapons[i].setRotation(180);
		//weapons[i].setColor(Color::Red);
		weaponB[i] = weapons[i].getGlobalBounds(); 
	}

	while (window.isOpen() && state == shop)
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		Event event;
		while (window.pollEvent(event))
		{
			//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed:
				window.close();
				break;
				//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					window.close();
				}
				break;
				//MOUSE EVENTS HERE
			case Event::MouseButtonPressed:
				if (event.mouseButton.button == Mouse::Left)
				{
					for (int i = 0; i < 3; i++)
					{
						if (shipB[i].contains(event.mouseButton.x, event.mouseButton.y))
						{
							switch (i)
							{
							case 0:
								if (player1.isRocketBought(1))
									chosenR = millitary;
								else if (player1.buyR(1))
									chosenR = millitary;
								else
									error.setString("Cannot buy rocket insufficient funds");
								break;
							case 1:
								if (player1.isRocketBought(2))
									chosenR = explorer;
								else if (player1.buyR(2))
									chosenR = explorer;
								else
									error.setString("Cannot buy rocket insufficient funds");								
								break;
							case 2:
								if (player1.isRocketBought(3))
									chosenR = alien;
								else if (player1.buyR(3))
									chosenR = alien;
								else
									error.setString("Cannot buy rocket insufficient funds");								
								break;
							}
						}
						else if (weaponB[i].contains(event.mouseButton.x, event.mouseButton.y))
						{
							switch (i)
							{
							case 0:
								if (player1.isWeaponBought(1))
									chosenW = gun;
								else if (player1.buyW(1))
									chosenW = gun;
								else
									error.setString("Cannot buy weapon insufficient funds");
								break;							
							case 1:
								if (player1.isWeaponBought(2))
									chosenW = laser;
								else if (player1.buyW(2))
									chosenW = laser;
								else
									error.setString("Cannot buy weapon insufficient funds");
								break;
							case 2:
								if (player1.isWeaponBought(3))
									chosenW = lightning;
								else if (player1.buyW(3))
									chosenW = lightning;
								else
									error.setString("Cannot buy weapon insufficient funds");
								break;
							}
						}
					}
					if (okayB.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
					{
						rocket1.setWT(chosenW);
						rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
						okayB.setFillColor(Color::Blue);
						state = menu;
					}
				}
					break;
			case Event::MouseMoved:
				cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
				break;

			}
		}

			switch (chosenR)
			{
			case millitary: 
				rocketT = "Millitary";
				break;
			case explorer: 
				rocketT = "Explorer";
				break;
			case alien:
				rocketT = "Alien";
				break;
			}

			switch (chosenW)
			{
			case gun: 
				weaponT = "Gun";
				break;
			case laser: 
				weaponT = "Laser";
				break;
			case lightning:
				weaponT = "Lightning";
				break;
			}

		currentFS = "Current Food: " + to_string(player1.getMeat());
		currentMS = "Current Coins: " + to_string(player1.getCoins());
		currentM.setString(currentMS);
		currentF.setString(currentFS);


		rocketNow.setString("Current Equipped Rocket: " + rocketT);
		weaponNow.setString("Current Equipped Weapon: " + weaponT);
		back1.spriteUp(time);

		window.clear();
		back1.drawOn(window);
		window.draw(info);
		window.draw(weaponNow);
		window.draw(rocketNow);
		window.draw(currentF); 
		window.draw(currentM);
		window.draw(error);
		window.draw(okayB); 
		window.draw(okayT);
		for (int i = 0; i < 3; i++)
		{
			if (player1.isRocketBought(i + 1))
				shipS[i].setString("OWNED");
			else
				shipS[i].setString("BUY NOW!");

			if (player1.isWeaponBought(i + 1))
				weaponS[i].setString("OWNED");
			else
				weaponS[i].setString("BUY NOW!");
			weaponS[i].setPosition(i * 384 + 150, 950.f);

			window.draw(weaponS[i]); 
			window.draw(prices[i]); 
			window.draw(shipS[i]);
			window.draw(weapons[i]); 
			window.draw(ships[i]);
		}
		window.draw(cursor);
		window.display();
	}

}

void gameF()
{
	while (window.isOpen() && state == game)
	{
		//clearAll();
		switch (level)
		{
		case 1: 
			transitionF(1);
            pointerslvl1();
			break;
        case 2:
			bossEntrance();
            bossF();
            break;
		case 3:
			transitionF(2);
			lvl2();
            break;
        case 4:
			bossEntrance();
			bossF();
			break;
        case 5:
			transitionF(3);
			lvl3();
            break;
        case 6:
			bossEntrance();
			bossF();
			if (state==game&&!dead1)
				transitionF(0);
			break;
		case 7: 
			state = results; 
			//resultsF();
			break;
		default:
			break;
		}
	}
}

void lvl1()
{
	rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
	rocket1.setSize(90.f, 120.f);
	rocket1.setWT(chosenW);
	rocket1.setSpeed(ROCKET_SPEED);

	beginM.play();
	back back1;
	Clock clock;
	Clock immuneC; 
	Clock respawnC; 
	float respawnT;
	float timeImmune;
	float time;
	Font font;
	string stat = "";
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif

	Text scoreT;
	scoreT.setFont(font);
	scoreT.setCharacterSize(30.f);
	scoreT.setOutlineThickness(2.f);
	scoreT.setFillColor(Color::Black);
	scoreT.setOutlineColor(Color::White);


	for (int i=0; i<4; i++)
		for (int j = 0; j < 10; j++)
		{
			chickens.push_back(chicken());
			chickens.at(chickens.size() - 1).activate(); 
			chickens.at(chickens.size() - 1).setType(normal, chicken1T, chicken2T, chicken3T, chicken4T);
			chickens.at(chickens.size() - 1).setSize(100.f);
			chickens.at(chickens.size() - 1).setPosition(100.f+j*chickens.at(chickens.size() - 1).getGlobalBounds().width, i*chickens.at(chickens.size() - 1).getGlobalBounds().height+100.f);
		}



	while (window.isOpen() && level==1 &&state==game)
	{
		respawnT = respawnC.getElapsedTime().asSeconds();
		timeImmune = immuneC.getElapsedTime().asSeconds();
		
		if (timeImmune < IMMUNITY_TIME)
			rocket1.Immune();
		else
			rocket1.deImmune();

		time = clock.getElapsedTime().asSeconds();
		clock.restart();
		stat = "";
		stat += "SCORE: " + to_string(player1.getScore());
		stat += " LIVES: " + to_string(rocket1.getLives());
		stat += " FOOD: " + to_string(player1.getMeat());
		stat += " COINS: " + to_string(player1.getCoins());
		stat += " IMMUNE: ";
		if (rocket1.isImmune())
			stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
		else
			stat += "NO";
		scoreT.setString(stat);
		scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);

		moveAll(time);
		handleGame(time);
		handleCollisions(immuneC);
		handleOut();
		handleDead(immuneC, respawnC, respawnT);
		updateAll();
		handleInactive();

		handleLevel();

		back1.spriteUp(time);
		window.clear();
		
		back1.drawOn(window);
		drawAll();
		window.draw(scoreT);
		window.display();
	}
}

void lvl2()
{
	rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
	rocket1.setSize(90.f, 120.f);
	rocket1.setWT(chosenW);
	rocket1.setSpeed(ROCKET_SPEED);

	beginM.play();
	back back1;
	Clock clock;
	Clock immuneC;
	Clock rockC;
	Clock respawnC; 
	float respawnT;
	int timeRock=0;
	int timeRock2=2005; 
	float timeImmune;
	float time;
	Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif


	string stat;

	Text scoreT;
	scoreT.setFont(font);
	scoreT.setCharacterSize(30.f);
	scoreT.setOutlineThickness(2.f);
	scoreT.setFillColor(Color::Black);
	scoreT.setOutlineColor(Color::White);
	//scoreT.setString(player1.getScore());
	scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);

	while (window.isOpen() && level == 3 && state == game)
	{
		respawnT = respawnC.getElapsedTime().asSeconds();
		if (timeRock2 - timeRock >= 700 && timeRock2<30000)
		{
			timeRock = timeRock2;
			for (int i = 0; i < 4; i++)
			{
				rocks.push_back(rock());
				rocks.at(rocks.size() - 1).setType(false, rockT);
				rocks.at(rocks.size() - 1).setSize(rand() % ROCK_SIZERANGE + ROCK_MINSIZE);
				rocks.at(rocks.size() - 1).setLP(rocks.at(rocks.size() - 1).getSize() / ROCK_MINSIZE * ROCKET_LIFE_POINTS);
				if (i < 2)
				{
					rocks.at(rocks.size() - 1).setPosition(rand() % (WIDTH / 2 - int(rocks.at(rocks.size() - 1).getGlobalBounds().width)) + WIDTH / 2, -rocks.at(rocks.size() - 1).getGlobalBounds().height);
					rocks.at(rocks.size() - 1).setSpeed(Vector2f(-100.f, 150.f));
				}
				else
				{
					rocks.at(rocks.size() - 1).setPosition(WIDTH + rocks.at(rocks.size() - 1).getGlobalBounds().width,  rand()%800);
					rocks.at(rocks.size() - 1).setSpeed(Vector2f(-100.f, 150.f));
				}
				rocks.at(rocks.size() - 1).setOrigin(rocks[i].getGlobalBounds().width / 2.f, rocks[i].getGlobalBounds().width / 2.f);
			}
	  }

		timeRock2 = rockC.getElapsedTime().asMilliseconds();
		timeImmune = immuneC.getElapsedTime().asSeconds();

		if (timeImmune < IMMUNITY_TIME)
			rocket1.Immune();
		else
			rocket1.deImmune();

		time = clock.getElapsedTime().asSeconds();
		clock.restart();
		stat = "";
		stat += "SCORE: " + to_string(player1.getScore());
		stat += " LIVES: " + to_string(rocket1.getLives());
		stat += " FOOD: " + to_string(player1.getMeat());
		stat += " COINS: " + to_string(player1.getCoins());
		stat += " IMMUNE: ";
		if (rocket1.isImmune())
			stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
		else
			stat += "NO";
		scoreT.setString(stat);
		scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);

		moveAll(time);
		handleGame(time);
		handleCollisions(immuneC);
		handleOut();
		handleDead(immuneC, respawnC, respawnT);
		updateAll();
		handleInactive();

		handleLevel();

		back1.spriteUp(time);
		window.clear();

		back1.drawOn(window);
		drawAll();
		window.draw(scoreT);
		window.display();
	}

}

void lvl3()
{
	rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
	rocket1.setSize(90.f, 120.f);
	rocket1.setWT(chosenW);
	rocket1.setSpeed(ROCKET_SPEED);

	beginM.play();
	back back1;
	Clock clock;
	Clock immuneC;
	Clock respawnC;
	float respawnT;
	float timeImmune;
	float time;
	Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif


	string stat;

	Text scoreT;
	scoreT.setFont(font);
	scoreT.setCharacterSize(30.f);
	scoreT.setOutlineThickness(2.f);
	scoreT.setFillColor(Color::Black);
	scoreT.setOutlineColor(Color::White);
	//scoreT.setString(player1.getScore());
	scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);

	for (int i = 0; i<4; i++)
		for (int j = 0; j < 5; j++)
		{
			chickens.push_back(chicken());
			chickens.at(chickens.size() - 1).activate();
			chickens.at(chickens.size() - 1).setType(trooper, chicken1T, chicken2T, chicken3T, chicken4T);
			chickens.at(chickens.size() - 1).setSize(100.f);
			chickens.at(chickens.size() - 1).setPosition(j*chickens.at(chickens.size() - 1).getGlobalBounds().width+100.f, i*chickens.at(chickens.size() - 1).getGlobalBounds().height + 100.f);
			chickens.at(chickens.size() - 1).setSpeed(NORMAL_SPEED);
		}

	for (int i = 0; i<4; i++)
		for (int j = 0; j <5; j++)
		{
			chickens.push_back(chicken());
			chickens.at(chickens.size() - 1).activate();
			chickens.at(chickens.size() - 1).setType(trooper, chicken1T, chicken2T, chicken3T, chicken4T);
			chickens.at(chickens.size() - 1).setSize(100.f);
			chickens.at(chickens.size() - 1).setPosition(WIDTH- (j*chickens.at(chickens.size() - 1).getGlobalBounds().width + 100.f), i*chickens.at(chickens.size() - 1).getGlobalBounds().height + 100.f);
			chickens.at(chickens.size() - 1).setSpeed(-NORMAL_SPEED);
		}



	while (window.isOpen() && level == 5 && state == game)
	{
		respawnT = respawnC.getElapsedTime().asSeconds();
		timeImmune = immuneC.getElapsedTime().asSeconds();

		if (timeImmune < IMMUNITY_TIME)
			rocket1.Immune();
		else
			rocket1.deImmune();

		time = clock.getElapsedTime().asSeconds();
		clock.restart();
		stat = "";
		stat += "SCORE: " + to_string(player1.getScore());
		stat += " LIVES: " + to_string(rocket1.getLives());
		stat += " FOOD: " + to_string(player1.getMeat());
		stat += " COINS: " + to_string(player1.getCoins());
		stat += " IMMUNE: ";
		if (rocket1.isImmune())
			stat += "YES" +to_string(IMMUNITY_TIME - int(timeImmune));
		else
			stat += "NO";
		scoreT.setString(stat);
		scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);
		moveAll(time);
		handleGame(time);
		handleCollisions(immuneC);
		handleOut();
		handleDead(immuneC, respawnC, respawnT);
		updateAll();
		handleInactive();

		handleLevel();

		back1.spriteUp(time);
		window.clear();

		back1.drawOn(window);
		drawAll();
		window.draw(scoreT);
		window.display();
	}

}

void bossF()
{
	boss.activate();
    rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
    rocket1.setSize(90.f, 120.f);
    rocket1.setWT(chosenW);
    rocket1.setSpeed(ROCKET_SPEED);

	boss.setType(mother, chicken1T, chicken2T, chicken3T, chicken4T);
	boss.setSize(500.f);
//	boss.setPosition(WIDTH / 2.F, HEIGHT / 2.F - boss.getSize() / 2);
    
    beginM.play();
    back back1;
    Clock clock;
    Clock immuneC;
	Clock respawnC;
	float respawnT;
    float timeImmune;
    float time;
    Font font;
    string stat = "";
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif
    
    Text scoreT;
    scoreT.setFont(font);
    scoreT.setCharacterSize(30.f);
    scoreT.setOutlineThickness(2.f);
    scoreT.setFillColor(Color::Black);
    scoreT.setOutlineColor(Color::White);
    
    
    while (window.isOpen() && (level== 2||level== 4||level== 6 || level == 8) && state==game)
    {
		respawnT = respawnC.getElapsedTime().asSeconds();
        timeImmune = immuneC.getElapsedTime().asSeconds();
        
        if (timeImmune < IMMUNITY_TIME)
            rocket1.Immune();
        else
            rocket1.deImmune();
        
        time = clock.getElapsedTime().asSeconds();
        clock.restart();
        stat = "";
        stat += "SCORE: " + to_string(player1.getScore());
        stat += " LIVES: " + to_string(rocket1.getLives());
        stat += " FOOD: " + to_string(player1.getMeat());
        stat += " COINS: " + to_string(player1.getCoins());
        stat += " IMMUNE: ";
        if (rocket1.isImmune())
            stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
        else
            stat += "NO";
        scoreT.setString(stat);
        scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);
        
		if (boss.drop(pickups, eggs, eggT, powerT, immuneT, heartT, coinT, foodT))
			chickLay.play();

        moveAll(time);
        handleGame(time);
        handleCollisions(immuneC);
        handleOut();
		handleDead(immuneC, respawnC, respawnT);
		updateAll();
        handleInactive();
        
        handleLevel();
        
        back1.spriteUp(time);
        window.clear();
        
        back1.drawOn(window);
        drawAll();
        window.draw(scoreT);
        window.display();
    }
}

void pauseF()
{
	back back1;
	Clock clock;
	float time;
	Font font;
	cursor.setTexture(rocket1T);
	cursor.setSize(65);
	cursor.setOrigin(cursor.getLocalBounds().width / 2.f, cursor.getLocalBounds().height / 2.f);
#ifdef __APPLE__
	if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
	{
		cout << "error loading font file";
	}
#else
	if (!font.loadFromFile("default.TTF"))
		cout << "error loading font in menu function" << endl;
#endif

	Text title;
	title.setFont(font);
	title.setCharacterSize(90.f);
	title.setOutlineThickness(4.5);
	title.setFillColor(Color::Black);
	title.setOutlineColor(Color::White);
	title.setString("GAME PAUSED");
	title.setPosition(WIDTH / 2 - title.getLocalBounds().width / 2.f, 10.f);
	//create items here [start game, highscores, signUp, logIn, Quit, shop]
	RectangleShape buttons[3];
	Text buttonT[3];
	FloatRect buttonB[3];
	buttonT[0].setString("Resume");
	buttonT[1].setString("Menu");
	buttonT[2].setString("Quit");

	for (int i = -1; i <= 1; i++)
	{
		buttons[i + 1].setFillColor(Color::Black);
		buttons[i + 1].setSize(Vector2f(500.f, 100.f));
		buttons[i + 1].setPosition(Vector2f(float(WIDTH) / 2.f - 250.f, float(HEIGHT) / 2.f + i*150.f + 100.f));

		buttonT[i + 1].setFont(font);
		buttonT[i + 1].setFillColor(Color::Black);
		buttonT[i + 1].setCharacterSize(40.f);
		buttonT[i + 1].setOutlineThickness(2.f);
		buttonT[i + 1].setOutlineColor(Color::White);
		buttonT[i + 1].setPosition(buttons[i + 1].getPosition().x + buttons[i + 1].getLocalBounds().width / 2 - buttonT[i + 1].getLocalBounds().width / 2, buttons[i + 1].getPosition().y + buttons[i + 1].getLocalBounds().height / 2 - buttonT[i + 1].getLocalBounds().height / 2);
		buttonB[i + 1] = buttons[i + 1].getGlobalBounds();
	}

	while (window.isOpen() && state == pause)
	{
		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		Event event;
		while (window.pollEvent(event))
		{
			//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed:
				window.close();
				break;
				//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					window.close();
				}
				break;
				//MOUSE EVENTS HERE
			case Event::MouseButtonPressed:
				if (event.mouseButton.button == Mouse::Left)
					for (int i = 0; i < 3; i++)
					{
						if (buttonB[i].contains(event.mouseButton.x, event.mouseButton.y))
						{
							buttons[i].setFillColor(Color::Blue);
							switch (i)
							{
							case 0:
								state = game;
								break;
							case 1:
								state = menu;
								clearAll();
								break;
							case 2:
								window.close();
								break;
							}
							break;
						}
						else

						{
							buttons[i].setFillColor(Color::Black);
							continue;
						}
					}
				break;
			case Event::MouseMoved:
				cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
				break;

			}
		}


		window.clear();
		back1.spriteUp(time);
		back1.drawOn(window);
		window.draw(title);
		for (int i = 0; i < 3; i++)
		{
			window.draw(buttons[i]);
			window.draw(buttonT[i]);
		}
		window.draw(cursor);
		window.display();

	}
}

void handleGame(float time)
{
	Event event;
	while (window.pollEvent(event))
	{
		//WINDOW EVENTS HERE
		switch (event.type)
		{
		case Event::Closed:
			window.close();
			break;
			//KEYBOARD EVENTS HERE
		case Event::KeyPressed:
			switch (event.key.code)
			{
			case Keyboard::Escape:
				state = pause;
				pauseF();
				break;
			case Keyboard::D:
			case Keyboard::Right:
				if (rocket1.getGlobalBounds().left+rocket1.getGlobalBounds().width<dBounds.left+dBounds.width)
				rocket1.move(rocket1.getSpeed().x*time, 0.f);
				break;
			case Keyboard::A:
			case Keyboard::Left:
				if (rocket1.getGlobalBounds().left>dBounds.left)
				rocket1.move(-rocket1.getSpeed().x*time, 0.f);
				break;
			case Keyboard::W:
			case Keyboard::Up:
				if (rocket1.getGlobalBounds().top > dBounds.top)
				rocket1.move(0.f, -rocket1.getSpeed().y*time);
				break;
			case Keyboard::S:
			case Keyboard::Down:
				if (rocket1.getGlobalBounds().top+rocket1.getGlobalBounds().height<dBounds.top+dBounds.height)
				rocket1.move(0.f, rocket1.getSpeed().y*time);
				break;
			case Keyboard::X:
				rocket1.IWP();
				break;
			case Keyboard::C:
				rocket1.DWP();
				break;
			case Keyboard::L:
				level++;
				break;
			case Keyboard::Space:
				if (rocket1.isActive())
				{
					rocket1.fire(bullets);
					updateBullets();
					fireM.play();
				}
				break;
			}
			break;
			//MOUSE EVENTS HERE
		case Event::MouseButtonPressed:
			if (event.mouseButton.button == Mouse::Left)
			{
				if (rocket1.isActive())
				{
					rocket1.fire(bullets);
					updateBullets();
					fireM.play();
				}
			}
			break;
		case Event::MouseMoved:
			if (dBounds.contains(event.mouseMove.x, event.mouseMove.y))
				rocket1.setPosition(event.mouseMove.x, event.mouseMove.y);
			else
				rocket1.setPosition(event.mouseMove.x, rocket1.getPosition().y);

			
			break;

		}
	}
	shield.setSize(3.f/2.f*rocket1.getGlobalBounds().width, 3.f / 2.f*rocket1.getGlobalBounds().height); //new
	rocket1.setOrigin(rocket1.getLocalBounds().width / 2.f, rocket1.getLocalBounds().height / 2.f);//new
	shield.setOrigin(shield.getLocalBounds().width / 2.f, shield.getLocalBounds().height / 2.f); //new
	shield.setPosition(rocket1.getPosition());
	//shield.setPosition(rocket1.getGlobalBounds().left - 30.f, rocket1.getGlobalBounds().top - 22.5);

}

void updateBullets()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		bullets[i].setType(chosenW, bullet1T, bullet2T, bullet3T);
	}
}
//when firing setType of all bullets

void handleDead(Clock &immuneC, Clock & respawnC, float &respawnT)
{
	for (int i = 0; i < rocks.size(); i++)
		if (rocks[i].isDead()&&rocks[i].isActive())
		{
			player1.addScore(rocks[i].getVal());
			int probability = rand() % 100;
			if (probability>90)
				rocks[i].releasePowerup(pickups, powerT);
			rocks[i].deactivate();
			rockDie.play();
		}

	for (int i = 0; i < chickens.size(); i++)
		if (chickens[i].isDead()&&chickens[i].isActive())
		{
			chickens[i].deactivate();
			player1.addScore(chickens[i].getVal());
		}

	if (boss.isDead()&&boss.isActive())
	{
		player1.addScore(boss.getVal());
		boss.deactivate();
	}

	if (rocket1.isDead())
	{
		if (!rocket1.gameOver())
		{
			if (rocket1.respawn())
			{
				respawnC.restart();
				rocket1.deactivate();
			}
			rocket1.Immune();
			immuneC.restart();
		}
		//if no more lives, game over
		else
		{
			overM.play();
			dead1 = true;
			level = 7;
		}
	}
	else
		if (respawnT > 2.f && respawnT < 2.1)
		{
			rocket1.activate();
			rocket1.Immune();
			immuneC.restart();
		}

	if (int(respawnT) % 10 == 0)
		omlettes.clear();


}

void handleLevel()
{
    if (dead1)
		level = 7;

	else if (chickens.empty() && rocks.empty()&&!boss.isActive()&&!split.isActive())
			level++;
}

void transitionF(int Level)
{
	clearAll(); 
	back back1;
	Clock clock;
	Clock timeC; 
	float timeT;
	float time;
	float sizeT=40.f;
	Font font;
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif

	bool flag = true; //as long as okay button is not pressed

	RectangleShape okayB;
	okayB.setFillColor(Color::Black);
	okayB.setSize(Vector2f(200.f, 100.f));
	okayB.setPosition(WIDTH - okayB.getGlobalBounds().width, HEIGHT - okayB.getGlobalBounds().height);

	Text okayT;
	okayT.setFont(font);
	okayT.setCharacterSize(40.f);
	okayT.setOutlineThickness(2.f);
	okayT.setFillColor(Color::Black);
	okayT.setOutlineColor(Color::White);
	okayT.setString("NEXT");
	okayT.setPosition(WIDTH - okayB.getGlobalBounds().width / 2 - okayT.getLocalBounds().width / 2, HEIGHT - okayB.getGlobalBounds().height / 2 - okayT.getLocalBounds().height / 2);

	Text info;
	info.setFont(font);
	info.setCharacterSize(40.f);
	info.setOutlineThickness(2.f);
	info.setFillColor(Color::Black);
	info.setOutlineColor(Color::White);
	if (Level!=0)
		info.setString("HEADING TO PLANET "+ to_string(Level));
	else
		info.setString("YOU SAVED THE WORLD");
	info.setPosition(WIDTH / 2.f - info.getLocalBounds().width / 2.f, HEIGHT / 2.f - info.getLocalBounds().height / 2.f);


	nextM.play();
	while (window.isOpen() && flag)
	{
		time = clock.getElapsedTime().asSeconds();
		timeT = timeC.getElapsedTime().asSeconds(); 

		clock.restart();

		Event event;
		while (window.pollEvent(event))
		{
			//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed:
				window.close();
				break;
				//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					window.close();
				}
				break;
				//MOUSE EVENTS HERE
			case Event::MouseButtonPressed:
				if (event.mouseButton.button == Mouse::Left)
				{

					if (okayB.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
					{
						flag = false;
					}
				}
				break;
			case Event::MouseMoved:
				cursor.setPosition(event.mouseMove.x, event.mouseMove.y);
				rocket1.setPosition(event.mouseMove.x, event.mouseMove.y);
				break;

			}
		}
		shield.setPosition(rocket1.getPosition());
		if (timeT > 3.f)
			flag = false;

		clearAll();
		sizeT += time*15.f;
		info.setCharacterSize(sizeT);
		info.setPosition(WIDTH / 2.f - info.getGlobalBounds().width / 2.f, HEIGHT / 2.f - info.getGlobalBounds().height / 2.f);

		back1.spriteUp(time);

		window.clear();
		back1.drawOn(window);
		window.draw(info);
		window.draw(okayB);
		window.draw(okayT);
		window.draw(rocket1);
		window.display();
	}
	nextM.stop();
}

void init()
{
	rocket1.activate();
	rocket1.setLives(3);
	rocket1.setLP(ROCKET_LIFE_POINTS);
	split.deactivate();
	boss.deactivate();
	rocket1.minPower();
	rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
	rocket1.setSize(90.f, 120.f);
	rocket1.setWT(chosenW);
	rocket1.setDmg(BULLET_DAMAGE);
	rocket1.setSpeed(ROCKET_SPEED);
	shield.setTexture(shieldT);
	shield.setSize(rocket1.getGlobalBounds().width, rocket1.getGlobalBounds().height); //new
	//shield.setSize(rocket1.getSize() + 60.f);
	level = 1; 
	dead1 = false;
	clearAll();
}
<<<<<<< HEAD

void bossEntrance()
{
	boss.activate();
	rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
	rocket1.setSize(90.f, 120.f);
	rocket1.setWT(chosenW);
	rocket1.setSpeed(ROCKET_SPEED);

	boss.setType(mother, chicken1T, chicken2T, chicken3T, chicken4T);
	boss.setSize(500.f);
	boss.setSpeed(Vector2f(0.f, 100.f));
	boss.setPosition(WIDTH / 2.f, - boss.getGlobalBounds().height);
	boss.setRotation(0);

	bool transe = true;
	back back1;
	Clock clock;
	Clock immuneC;
	Clock respawnC;
	float respawnT;
	float timeImmune;
	float time;
	Font font;
	string stat = "";
#ifdef __APPLE__
	if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
	{
		cout << "error loading font file";
	}
#else
	if (!font.loadFromFile("default.TTF"))
		cout << "error loading font in menu function" << endl;
#endif

	Text scoreT;
	scoreT.setFont(font);
	scoreT.setCharacterSize(30.f);
	scoreT.setOutlineThickness(2.f);
	scoreT.setFillColor(Color::Black);
	scoreT.setOutlineColor(Color::White);

	switch (level)
	{
	case 2: 
		bubble.setTexture(bubble1T);
		bubble.setSize(300);
		bubble.setOrigin(bubble.getGlobalBounds().width, bubble.getGlobalBounds().height);
		break;
	case 4:
		bubble.setTexture(bubble2T);
		bubble.setSize(300);
		bubble.setOrigin(bubble.getGlobalBounds().width, bubble.getGlobalBounds().height);
		break;
	case 6:
		bubble.setTexture(bubble3T);
		bubble.setSize(300);
		bubble.setOrigin(bubble.getGlobalBounds().width, bubble.getGlobalBounds().height);
		break;
	}


	while (window.isOpen() &&transe&& (level == 2 || level == 4 || level == 6 ) && state == game)
	{
		respawnT = respawnC.getElapsedTime().asSeconds();
		timeImmune = immuneC.getElapsedTime().asSeconds();
		immuneC.restart();

		if (timeImmune < IMMUNITY_TIME)
			rocket1.Immune();
		else
			rocket1.deImmune();

		time = clock.getElapsedTime().asSeconds();
		clock.restart();

		stat = "";
		stat += "SCORE: " + to_string(player1.getScore());
		stat += " LIVES: " + to_string(rocket1.getLives());
		stat += " FOOD: " + to_string(player1.getMeat());
		stat += " COINS: " + to_string(player1.getCoins());
		stat += " IMMUNE: ";
		if (rocket1.isImmune())
			stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
		else
			stat += "NO";
		scoreT.setString(stat);
		scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);

		stat = "";
		stat += "SCORE: " + to_string(player1.getScore());
		stat += " LIVES: " + to_string(rocket1.getLives());
		stat += " FOOD: " + to_string(player1.getMeat());
		stat += " COINS: " + to_string(player1.getCoins());
		stat += " IMMUNE: ";
		if (rocket1.isImmune())
			stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
		else
			stat += "NO";
		scoreT.setString(stat);
		scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);
		if (boss.getGlobalBounds().top + boss.getGlobalBounds().height > dBounds.top)
			boss.setSpeed(STATIONARY);

		if (respawnT > 10.f)
			transe = false;


		Event event;
		while (window.pollEvent(event))
		{
			//WINDOW EVENTS HERE
			switch (event.type)
			{
			case Event::Closed:
				window.close();
				break;
				//KEYBOARD EVENTS HERE
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Escape:
					state = pause;
					pauseF();
					break;
				case Keyboard::D:
				case Keyboard::Right:
					if (rocket1.getGlobalBounds().left + rocket1.getGlobalBounds().width<dBounds.left + dBounds.width)
						rocket1.move(rocket1.getSpeed().x*time, 0.f);
					break;
				case Keyboard::A:
				case Keyboard::Left:
					if (rocket1.getGlobalBounds().left>dBounds.left)
						rocket1.move(-rocket1.getSpeed().x*time, 0.f);
					break;
				case Keyboard::W:
				case Keyboard::Up:
					if (rocket1.getGlobalBounds().top > dBounds.top)
						rocket1.move(0.f, -rocket1.getSpeed().y*time);
					break;
				case Keyboard::S:
				case Keyboard::Down:
					if (rocket1.getGlobalBounds().top + rocket1.getGlobalBounds().height<dBounds.top + dBounds.height)
						rocket1.move(0.f, rocket1.getSpeed().y*time);
					break;
				case Keyboard::X:
					rocket1.IWP();
					break;
				case Keyboard::C:
					rocket1.DWP();
					break;
				case Keyboard::L:
					level++;
					break;
				}
				break;
				//MOUSE EVENTS HERE

			case Event::MouseMoved:
				if (dBounds.contains(event.mouseMove.x, event.mouseMove.y))
					rocket1.setPosition(event.mouseMove.x, event.mouseMove.y);
				else
					rocket1.setPosition(event.mouseMove.x, rocket1.getPosition().y);


				break;

			}
		}
		shield.setSize(3.f / 2.f*rocket1.getGlobalBounds().width, 3.f / 2.f*rocket1.getGlobalBounds().height); //new
		rocket1.setOrigin(rocket1.getLocalBounds().width / 2.f, rocket1.getLocalBounds().height / 2.f);//new
		shield.setOrigin(shield.getLocalBounds().width / 2.f, shield.getLocalBounds().height / 2.f); //new
		shield.setPosition(rocket1.getPosition());
		bubble.setPosition(rocket1.getPosition().x, rocket1.getPosition().y-rocket1.getGlobalBounds().height/2.f);

		
		moveAll(time);
		
		updateAll();
		handleInactive();

		back1.spriteUp(time);
		window.clear();

		back1.drawOn(window);
		if (respawnT>5.f)
			window.draw(bubble);
		drawAll();
		window.draw(scoreT);
		window.display();
	}

}
//Presented by Diaa and Hegazy
=======
void pointerslvl1()
{
    rocket1.setRT(chosenR, rocket1T, rocket2T, rocket3T);
    rocket1.setSize(90.f, 120.f);
    rocket1.setWT(chosenW);
    rocket1.setSpeed(ROCKET_SPEED);
    
    beginM.play();
    back back1;
    Clock clock;
    Clock immuneC;
    Clock respawnC;
    float respawnT;
    float timeImmune;
    float time;
    Font font;
    string stat = "";
    
#ifdef __APPLE__
    if (!font.loadFromFile(resourcePath() + "sansation.ttf"))
    {
        cout << "error loading font file";
    }
#else
    if (!font.loadFromFile("default.TTF"))
        cout << "error loading font in menu function" << endl;
#endif
    
    Text scoreT;
    scoreT.setFont(font);
    scoreT.setCharacterSize(30.f);
    scoreT.setOutlineThickness(2.f);
    scoreT.setFillColor(Color::Black);
    scoreT.setOutlineColor(Color::White);
    
    chicken* *p;
    p = new chicken*[4];
    for(int i = 0; i < 4; i++)
    {
        *p = new chicken[10];
        p++;
    }
    p = p - 4;
    
    for (int i=0; i<4; i++)
        for (int j = 0; j < 10; j++)
        {
            (*(*(p +i) + j)).activate();
            (*(*(p +i) + j)).setType(normal, chicken1T, chicken2T, chicken3T, chicken4T);
            (*(*(p +i) + j)).setSize(100.f);
            (*(*(p +i) + j)).setPosition(100.f+j*(*(*(p +i) + j)).getGlobalBounds().width, i*(*(*(p +i) + j)).getGlobalBounds().height+100.f);
        }
    
    while (window.isOpen() && level==1 &&state==game)
    {
        respawnT = respawnC.getElapsedTime().asSeconds();
        timeImmune = immuneC.getElapsedTime().asSeconds();
        
        if (timeImmune < IMMUNITY_TIME)
            rocket1.Immune();
        else
            rocket1.deImmune();
        
        time = clock.getElapsedTime().asSeconds();
        clock.restart();
        stat = "";
        stat += "SCORE: " + to_string(player1.getScore());
        stat += " LIVES: " + to_string(rocket1.getLives());
        stat += " FOOD: " + to_string(player1.getMeat());
        stat += " COINS: " + to_string(player1.getCoins());
        stat += " IMMUNE: ";
        
        if (rocket1.isImmune())
            stat += "YES" + to_string(IMMUNITY_TIME - int(timeImmune));
        else
            stat += "NO";
        scoreT.setString(stat);
        scoreT.setPosition(WIDTH / 2.f - scoreT.getLocalBounds().width / 2.f, 20.f);
        
        //move
        for (int i = 0; i < eggs.size(); i++)
            if (eggs[i].isActive())
                eggs[i].move(eggs[i].getSpeed()*time);
        
        for (int i = 0; i < pickups.size(); i++)
            if (pickups[i].isActive())
                pickups[i].move(pickups[i].getSpeed()*time);
        
        for (int i = 0; i < bullets.size(); i++)
            if (bullets[i].isActive())
            {
                bullets[i].move(-bullets[i].getSpeed()*time);
                if (rocket1.getWT() == lightning)
                    bullets[i].setPosition(rocket1.getPosition().x, rocket1.getPosition().y);
            }
        
        for (int i = 0; i < rocks.size(); i++)
            if (rocks[i].isActive())
            {
                rocks[i].move(rocks[i].getSpeed()*time);
            }
        
        for (int i = 0; i < 4; i++)
            for(int j=0; j < 10; j++)
            if ((*(*(p +i) + j)).isActive())
                (*(*(p +i) + j)).move((*(*(p +i) + j)).getSpeed()*time);
        
        

        handleGame(time);
        
        //handle collision
        //bullets with chickens
        for (int i = 0; i < bullets.size(); i++)
        {
            //exits the loop if bullet deactivates
            for (int k = 0; k < 4; k++)
            for (int j = 0; j < 10 && bullets[i].isActive(); j++)
                if ((*(*(p+k) + j)).isActive() && (*(*(p +k) + j)).isHit(bullets[i]))
                {
                    bullets[i].deactivate();
                    player1.addScore((*(*(p + k) + j)).getVal());
                    (*(*(p+k) + j)).setLP(0);
                    if(!(*(*(p+k) + j)).drop(pickups, eggs, eggT, powerT, immuneT, heartT, coinT, foodT));
                        chickHit.play();
                }
        }
        
        //eggs with rocket
        for (int i = 0; i < eggs.size(); i++)
        {
            //Do not let rocket die if it's in immunity or if the egg is inactive
            if (eggs[i].isActive() &&rocket1.isActive()&& rocket1.isHit(eggs[i]) && !rocket1.isImmune())
            {
                rocketDie.play();
                rocket1.die();
                eggs[i].deactivate();
                //if there are remaining lives, respawn and make immunity [initial immunity]
                
            }
        }
        
        //chickens with rocket
        for (int i=0; i<4; i++)
            for (int j = 0; j < 10; j++)
        {
            //Do not let rocket die if it's in immunity or if the chickens is inactive
            if ((*(*(p +i) + j)).isActive() && rocket1.isActive() && rocket1.isHit((*(*(p +i) + j))) && !rocket1.isImmune())
            {
                rocketDie.play();
                rocket1.die();
                (*(*(p +i) + j)).setLP(0);
                (*(*(p +i) + j)).drop(pickups, eggs, eggT, powerT, immuneT, heartT, coinT, foodT);
            }
        }
        
        //prizes with rocket
        for (int i = 0; i < pickups.size(); i++)
        {
            if (pickups[i].isHit(rocket1) && rocket1.isActive() && pickups[i].isActive())
            {
                switch (pickups[i].getPrizeType())
                {
                    case immune:
                        beginM.play();
                        immuneC.restart(); 
                        rocket1.Immune();
                        break;
                    case power:
                        powerM.play();
                        rocket1.IWP();
                        break;
                    case food:
                        foodM.play();
                        player1.addFood(MEAT_VAL);
                        break;
                    case life:
                        beginM.play();
                        rocket1.oneUp();
                        break;
                    case money:
                        
                        foodM.play();
                        player1.addCoins(COIN_VAL);
                        break;
                    default:
                        break;
                }
                pickups[i].deactivate();
            }
        }
        
        
        
        
        //handle inactive
        //eggs
        for (int i = 0; i < eggs.size(); i++)
            if (!eggs[i].isActive())
            {
                eggs.erase(eggs.begin() + i);
                i--;
            }
        //pickups
        for (int i = 0; i < pickups.size(); i++)
            if (!pickups[i].isActive())
            {
                player1.addScore(pickups[i].getVal());
                pickups.erase(pickups.begin() + i);
                i--;
            }
        //bullets
        for (int i = 0; i < bullets.size(); i++)
            if (!bullets[i].isActive())
            {
                bullets.erase(bullets.begin() + i);
                i--;
            }

        
        //handle dead
        for (int i = 0; i < 4; i++)
            for(int j=0; j < 10; j++)
            if ((*(*(p +i) + j)).isDead()&&(*(*(p +i) + j)).isActive())
            {
                (*(*(p +i) + j)).deactivate();
                //player1.addScore((*(*(p +i) + j)).getVal());
            }
        
        if (rocket1.isDead())
        {
            if (!rocket1.gameOver())
            {
                if (rocket1.respawn())
                {
                    respawnC.restart();
                    rocket1.deactivate();
                }
                rocket1.Immune();
                immuneC.restart();
            }
            //if no more lives, game over
            else
            {
                overM.play();
                dead1 = true;
                level = 7;
            }
        }
        else
            if (respawnT > 2.f && respawnT < 2.1)
            {
                rocket1.activate();
                rocket1.Immune();
                immuneC.restart();
            }
        
        if (int(respawnT) % 10 == 0)
            omlettes.clear();

        
        //update all
        for (int i = 0; i < 4; i++)
            for(int j=0; j < 10; j++)
        {
            (*(*(p +i) + j)).updateChicken(chicken1T, chicken2T, chicken3T, chicken4T, chicken5T);
            if((*(*(p +i) + j)).isActive() && !(*(*(p +i) + j)).isDead())
            if ((*(*(p +i) + j)).drop(pickups, eggs, eggT, powerT, immuneT, heartT, coinT, foodT))
                chickLay.play();
        }
        
        
        //handle Out
        //chickens
        for (int i = 0; i < 4; i++)
            for(int j=0; j < 10; j++)
            if ((*(*(p +i) + j)).isActive() && ((*(*(p +i) + j)).getGlobalBounds().left<0.f || (*(*(p +i) + j)).getGlobalBounds().left+ (*(*(p +i) + j)).getGlobalBounds().width>WIDTH))
            {
                        if((*(*(p +i) + j)).isActive())
                           (*(*(p +i) + j)).setSpeed(-(*(*(p +i) + j)).getSpeed());
                      // break;
            }
        
        //eggs
        for (int i = 0; i < eggs.size(); i++)
        {
            if (eggs[i].isActive() && eggs[i].getPosition().y >= HEIGHT - eggs[i].getGlobalBounds().height)
            {
                eggs[i].deactivate();
                eggDie.play();
                releaseYolk(eggs[i]);
            }
            else if (eggs[i].isActive() && (eggs[i].getGlobalBounds().left<0.f || eggs[i].getGlobalBounds().left + eggs[i].getGlobalBounds().width>WIDTH))
                eggs[i].setSpeed(Vector2f(-eggs[i].getSpeed().x, eggs[i].getSpeed().y));
            
        }
        //pickups
        for (int i = 0; i < pickups.size(); i++)
        {
            if (pickups[i].getPrizeType() == food && pickups[i].isActive() && (pickups[i].getGlobalBounds().left<0.f|| pickups[i].getGlobalBounds().left+ pickups[i].getGlobalBounds().width>WIDTH))
            {
                pickups[i].setSpeed(Vector2f(-pickups[i].getSpeed().x, pickups[i].getSpeed().y));
            }
            
            else if (pickups[i].isActive() && pickups[i].outBounds(wBounds))
                pickups[i].deactivate();
        }
        
        //bullets
        for (int i = 0; i < bullets.size(); i++)
            if (bullets[i].isActive() && bullets[i].outBounds(wBounds))
                bullets[i].deactivate();
    
        handlePointerlvl(p);
        
        back1.spriteUp(time);
        window.clear();
        
        back1.drawOn(window);
        
        //draw all
        for (int i = 0; i < rocks.size(); i++)
            if (rocks[i].isActive())
                window.draw(rocks[i]);
        
        for (int i = 0; i < 4; i++)
            for(int j=0; j < 10; j++)
            if ((*(*(p +i) + j)).isActive())
                window.draw((*(*(p +i) + j)));
        
        for (int i = 0; i < eggs.size(); i++)
            if (eggs[i].isActive())
                window.draw(eggs[i]);
        
        for (int i = 0; i < pickups.size(); i++)
            if (pickups[i].isActive())
                window.draw(pickups[i]);
        
        for (int i = 0; i < bullets.size(); i++)
            if (bullets[i].isActive())
                window.draw(bullets[i]);
        
        for (int i = 0; i < omlettes.size(); i++)
            if (omlettes[i].isActive())
                window.draw(omlettes[i]);
        
        if (rocket1.isActive())
            window.draw(rocket1);
        
        if (rocket1.isImmune())
            window.draw(shield);
        
       
        
        window.draw(scoreT);
        window.display();
    }
}
void handlePointerlvl(chicken* *p)
{
    bool flag = true;
    for (int i = 0; i < 4; i++)
        for(int j=0; j < 10; j++)
            if((*(*(p +i) + j)).isActive())
                flag = false;
    if(flag)
        level++;
}

>>>>>>> 416914b11540d331d73da03ef357977ed4986aaa
