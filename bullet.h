#pragma once
#ifndef bullet_h
#define bullet_h
#include "object.h"
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
using namespace std;
using namespace sf;

class bullet : public object
{
private: 
	Wtype type;
public: 
	bullet();
	void setType(Wtype chosen, Texture &bullet1, Texture &bullet2, Texture &bullet3);
	Wtype getType() const;
};
#endif // !bullet_h