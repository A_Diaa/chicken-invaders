#include "object.h"
#include <iostream>
#include "bullet.h"
#include <string>
#include <SFML/Graphics.hpp>
using namespace std;
using namespace sf;



bullet::bullet()
{
	lifepoints = BULLET_LIFE_POINTS;
	damage = BULLET_DAMAGE;
	//bullet::setSpeed(Vector2f(0, BULLET_YGAIN));
	active = true;
}


void bullet::setType(Wtype chosen, Texture &bullet1, Texture &bullet2, Texture &bullet3)
{
	type = chosen;
	switch (chosen)
	{
	case gun:
	{
        bullet::setImg(bullet1);
		bullet::setSize(40);
		break;
	}
	case laser:
	{
        bullet::setImg(bullet2);
		bullet::setSize(10.f, 60.f);
		break;
	}
	case lightning:
	{
        bullet::setImg(bullet3);
		bullet::setSize(30.f, 700.f);
		break;
	}
	}
}

Wtype bullet::getType() const
{
	return type;
}
